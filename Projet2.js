// Constructeur de tuiles du labyrinthe
function Tuile(t)
{
	 for(this.typeCase = 0; this.typeCase<this.types.length; this.typeCase++)
	   if(this.types[this.typeCase]==t)
	     break;
	 if(this.typeCase==this.types.length)
    throw "Type " + t + " invalide !";
}
// Les 10 types de case:
Tuile.prototype.types = ["NS", "EO", "OS", "ES", "EN", "NO", "EOS", "ENS", "ENO", "NOS"];
// Table pour le pivotage (chaque élément contient l'indice du typeCase pivoté de 90°) :
// une tuile de ce type ----->   0, 1, 2, 3, 4, 5, 6, 7, 8, 9    devient, par pivotage de 90°,
Tuile.prototype.tablePivotage = [1, 0, 3, 4, 5, 2, 7, 8, 9, 6];
Tuile.prototype.getType = function()
{
	 return this.types[this.typeCase];
}
// Pivote la tuile de 90 dégrés en sense inverse des aiguilles de la montre:
Tuile.prototype.pivoter90 = function()
{
	 this.typeCase = this.tablePivotage[this.typeCase];
}
// Sérialisation d'une tuile : la seule information importante est le type (Cf. Exercice 1)
Tuile.prototype.toJSON = function()
{
	 return { typeCase: this.typeCase };
}
Tuile.prototype.fromJSON = function(o)
{
	 this.typeCase = o.typeCase;
}

 
// Constructeur de pions :
function Pion(i, j, num)
{
	 this.i = i;
	 this.j = j;
	 this.num = num
}
Pion.prototype.i = 0;
Pion.prototype.j = 0;
Pion.prototype.num = 0;
// Constructeur de tresor :
function Tresor(i, j, num)
{
	 this.i = i;
	 this.j = j;
	 this.num = num
}
Tresor.prototype.i = 0;
Tresor.prototype.j = 0;
Tresor.prototype.num = 0;

// Constructeur du labyrinthe
// L'objet "labyrinthe", avec la l minuscule, est créé dans la page HTML
function Labyrinthe(n,nbrJ)
{
	 this.n = n;
	 this.nbrJ = nbrJ;
	 this.B = "True";
	this.V = "False";
	this.R = "False";
	this.J = "False";
	this.fait = "False";
	this.mouvement = "True";
	this.gagnant = "False";
	 if(n % 2 == 0 || n<2)
	   throw "Taille du tableau invalide : " + n + " !";
	 var h = (n - 1)/2;
	 // Préparons l'ensemble des tuiles mobiles :
	 tuilesMobiles = new Array(n*n - (h + 1)*(h + 1) + 1);
	 var i = 0;
	 for(j = 0; j<4*h; j++)
	   tuilesMobiles[i++] = new Tuile("NS");
	 for(j = 0; j<2*h; j++)
	   tuilesMobiles[i++] = new Tuile("EOS");
	 while(i<tuilesMobiles.length)
	   tuilesMobiles[i++] = new Tuile("OS");
	 // Créons le tableau n x n :
	 this.cases = new Array(n*n);
	 // Remplissons le tableau...
	 for(i = 0; i<n*n; i++)
	   this.cases[i] = new Tuile("NS");
	 // Les 4 coins :
	 this.cell(0, 0, new Tuile("ES"));
	 this.cell(0, n - 1, new Tuile("OS"));
	 this.cell(n - 1, n - 1, new Tuile("NO"));
	 this.cell(n - 1, 0, new Tuile("EN"));
	 // Les tuiles fixes des marges :
	 for(i = 1; i<h; i++)
	 {
	 	  this.cell(0, 2*i, new Tuile("EOS"));
	 	  this.cell(2*i, n - 1, new Tuile("NOS"));
	 	  this.cell(n - 1, 2*i, new Tuile("ENO"));
	 	  this.cell(2*i, 0, new Tuile("ENS"));
	 	}	
	 	// Les tuiles fixes internes :
	 	for(i = 1; i<h; i++)
	 	  for(j = 1; j<h; j++)
	 	  {
	 	  	  if(j > i && j <= h - i)
	 	  	    this.cell(2*i, 2*j, new Tuile("EOS"));
	 	  	  else if(j >= i && j > h - i)
	 	  	    this.cell(2*i, 2*j, new Tuile("NOS"));
	 	  	  else if(j < i && j >= h - i)
	 	  	    this.cell(2*i, 2*j, new Tuile("ENO"));
	 	  	  else
	 	  	    this.cell(2*i, 2*j, new Tuile("ENS"));
	 	  	}
  // Plaçons les tuiles mobiles :
  var l = tuilesMobiles.length;
  for(i = 0; i<n - 1; i++)
    for(j = 0; j<n - 1; j++)
      if(i % 2 != 0 || j % 2 != 0)
      {
      	 // tirage à sort, sans remise, d'une tuile mobile :
      	 r = Math.floor(Math.random()*l--);
      	 this.cell(i, j, tuilesMobiles[r]);
      	 tuilesMobiles[r] = tuilesMobiles[l];
      	 
      	 // pivotage aléatoire de la tuile tirée à sort :
      	 p = Math.floor(Math.random()*4);
      	 while(p-- > 0)
      	   this.cell(i, j).pivoter90();
      }
  this.tuileEnTrop = tuilesMobiles[0];
  if (nbrJ == 1){
  	this.pionB = new Pion(0, 0,1);
	this.pionV = new Pion(-3, -3,2);
	this.pionJ = new Pion(-3, -3,3);
  	this.pionR = new Pion(-3, -3,4);  	
  	}
  else if (nbrJ == 2){
  	this.pionB = new Pion(0, 0,1);
	this.pionV = new Pion(0, n-1,2);
	this.pionJ = new Pion(-3, -3,3);
  	this.pionR = new Pion(-3, -3,4);  	
  	}
  else if (nbrJ == 3){
  	this.pionB = new Pion(0, 0,1);
	this.pionV = new Pion(0, n-1,2);
	this.pionJ = new Pion(-3, -3,3);
  	this.pionR = new Pion(n-1, n-1,4);  	
  	}
  else if (nbrJ == 4){
  	this.pionB = new Pion(0, 0,1);
	this.pionV = new Pion(0, n-1,2);
	this.pionJ = new Pion(n-1, 0,3);
  	this.pionR = new Pion(n-1, n-1,4);  	
  	}
  this.tresor0 = new Tresor(0,2,0);
  this.tresor1 = new Tresor(0,4,1);
  this.tresor2 = new Tresor(2,0,2);
  this.tresor3 = new Tresor(4,0,3);
  this.tresor4 = new Tresor(n-1,2,4);
  this.tresor5 = new Tresor(n-1,4,5);
  this.tresor6 = new Tresor(2,n-1,6);
  this.tresor7 = new Tresor(4,n-1,7);
  this.tresor8 = new Tresor(2,2,8);
  this.tresor9 = new Tresor(2,4,9);
  this.tresor10 = new Tresor(4,2,10);
  this.tresor11 = new Tresor(4,4,11);
  x12 = Math.floor(Math.random()*(n-1));
  y12 = Math.floor(Math.random()*(n-1));
  while ((x12 == 4 && y12 == 4) || (x12 == 4 && y12 == 2) || (x12 == 2 && y12 == 4) || (x12 == 2 && y12 == 2) || (x12 == 4 && y12 == n-1) || (x12 == 2 && y12 == n-1) || (x12 == n-1 && y12 == 4) || (x12 == n-1  && y12 == 2) || (x12 == 4 && y12 == 0) || (x12 == 2 && y12 == 0) || (x12 == 0 && y12 == 4) || (x12 == 0 && y12 == 2) || (x12 == 0 && y12 == 0) || (x12 == 0 && y12 == n-1) || (x12 == n-1 && y12 == 0) || (x12 == n-1 && y12 == n-1)){
	x12 = Math.floor(Math.random()*(n-1));
  y12 = Math.floor(Math.random()*(n-1));  	
  	}
  this.tresor12 = new Tresor(x12,y12,12);
  x13 = Math.floor(Math.random()*(n-1));
  y13 = Math.floor(Math.random()*(n-1));
  while ((x13 == x12 && y13 == y12) || (x13 == 4 && y13 == 4) || (x13 == 4 && y13 == 2) || (x13 == 2 && y13 == 4) || (x13 == 2 && y13 == 2) || (x13 == 4 && y13 == n-1) || (x13 == 2 && y13 == n-1) || (x13 == n-1 && y13 == 4) || (x13 == n-1  && y13 == 2) || (x13 == 4 && y13 == 0) || (x13 == 2 && y13 == 0) || (x13 == 0 && y13 == 4) || (x13 == 0 && y13 == 2) || (x13 == 0 && y13 == 0) || (x13 == 0 && y13 == n-1) || (x13 == n-1 && y13 == 0) || (x13 == n-1 && y13 == n-1)){
	x13 = Math.floor(Math.random()*(n-1));
  y13 = Math.floor(Math.random()*(n-1));  	
  	}
  this.tresor13 = new Tresor(x13,y13,13);
  x14 = Math.floor(Math.random()*(n-1));
  y14 = Math.floor(Math.random()*(n-1));
  while ((x14 == x13 && y14 == y13) || (x14 == x12 && y14 == y12) || (x14 == 4 && y14 == 4) || (x14 == 4 && y14 == 2) || (x14 == 2 && y14 == 4) || (x14 == 2 && y14 == 2) || (x14 == 4 && y14 == n-1) || (x14 == 2 && y14 == n-1) || (x14 == n-1 && y14 == 4) || (x14 == n-1  && y14 == 2) || (x14 == 4 && y14 == 0) || (x14 == 2 && y14 == 0) || (x14 == 0 && y14 == 4) || (x14 == 0 && y14 == 2) || (x14 == 0 && y14 == 0) || (x14 == 0 && y14 == n-1) || (x14 == n-1 && y14 == 0) || (x14 == n-1 && y14 == n-1)){
	x14 = Math.floor(Math.random()*(n-1));
  y14 = Math.floor(Math.random()*(n-1));  	
  	}
  this.tresor14 = new Tresor(x14,y14,14);
  x15 = Math.floor(Math.random()*(n-1));
  y15 = Math.floor(Math.random()*(n-1));
  while ((x15 == x14 && y15 == y14) || (x15 == x13 && y15 == y13) || (x15 == x12 && y15 == y12) || (x15 == 4 && y15 == 4) || (x15 == 4 && y15 == 2) || (x15 == 2 && y15 == 4) || (x15 == 2 && y15 == 2) || (x15 == 4 && y15 == n-1) || (x15 == 2 && y15 == n-1) || (x15 == n-1 && y15 == 4) || (x15 == n-1  && y15 == 2) || (x15 == 4 && y15 == 0) || (x15 == 2 && y15 == 0) || (x15 == 0 && y15 == 4) || (x15 == 0 && y15 == 2) || (x15 == 0 && y15 == 0) || (x15 == 0 && y15 == n-1) || (x15 == n-1 && y15 == 0) || (x15 == n-1 && y15 == n-1)){
	x15 = Math.floor(Math.random()*(n-1));
   y15 = Math.floor(Math.random()*(n-1));  	
  	}
  this.tresor15 = new Tresor(x15,y15,15);
  x16 = Math.floor(Math.random()*(n-1));
  y16 = Math.floor(Math.random()*(n-1));
  while ((x16 == x15 && y16 == y15) || (x16 == x14 && y16 == y14) || (x16 == x13 && y16 == y13) || (x16 == x12 && y16 == y12) || (x16 == 4 && y16 == 4) || (x16 == 4 && y16 == 2) || (x16 == 2 && y16 == 4) || (x16 == 2 && y16 == 2) || (x16 == 4 && y16 == n-1) || (x16 == 2 && y16 == n-1) || (x16 == n-1 && y16 == 4) || (x16 == n-1  && y16 == 2) || (x16 == 4 && y16 == 0) || (x16 == 2 && y16 == 0) || (x16 == 0 && y16 == 4) || (x16 == 0 && y16 == 2) || (x16 == 0 && y16 == 0) || (x16 == 0 && y16 == n-1) || (x16 == n-1 && y16 == 0) || (x16 == n-1 && y16 == n-1)){
	x16 = Math.floor(Math.random()*(n-1));
  y16 = Math.floor(Math.random()*(n-1));  	
  	}
  this.tresor16 = new Tresor(x16,y16,16);
  this.tresors = [this.tresor0,this.tresor1,this.tresor2,this.tresor3,this.tresor4,this.tresor5,this.tresor6,this.tresor7,this.tresor8,this.tresor9,this.tresor10,this.tresor11,this.tresor12,this.tresor13,this.tresor14,this.tresor15,this.tresor16];
ct1 = Math.floor((Math.random()*this.tresors.length));
this.J1T1 = this.tresors[ct1];
ct2 = Math.floor((Math.random()*this.tresors.length));
while (ct2 == ct1){
	ct2 = Math.floor((Math.random()*this.tresors.length));
	}
this.J1T2 = this.tresors[ct2];
ct3 = Math.floor((Math.random()*this.tresors.length));
while (ct3 == ct1 || ct3 == ct2){
	ct3 = Math.floor((Math.random()*this.tresors.length));
	}
this.J1T3 = this.tresors[ct3];
ct4 = Math.floor((Math.random()*this.tresors.length));
while (ct4 == ct1 || ct4 == ct2 || ct4 == ct3){
	ct4 = Math.floor((Math.random()*this.tresors.length));
	}
this.J2T1 = this.tresors[ct4];
ct5 = Math.floor((Math.random()*this.tresors.length));
while (ct5 == ct1 || ct5 == ct2 || ct5 == ct3 || ct5 == ct4){
	ct5 = Math.floor((Math.random()*this.tresors.length));
	}
this.J2T2 = this.tresors[ct5];
ct6 = Math.floor((Math.random()*this.tresors.length));
while (ct6 == ct1 || ct6 == ct2 || ct6 == ct3 || ct6 == ct4 || ct6 == ct5){
	ct6 = Math.floor((Math.random()*this.tresors.length));
	}
this.J2T3 = this.tresors[ct6];
ct7 = Math.floor((Math.random()*this.tresors.length));
while (ct7 == ct1 || ct7 == ct2 || ct7 == ct3 || ct7 == ct4 || ct7 == ct5 || ct7 == ct6){
	ct7 = Math.floor((Math.random()*this.tresors.length));
	}
this.J3T1 = this.tresors[ct7];
ct8 = Math.floor((Math.random()*this.tresors.length));
while (ct8 == ct1 || ct8 == ct2 || ct8 == ct3 || ct8 == ct4 || ct8 == ct5 || ct8 == ct6 || ct8 == ct7){
	ct8 = Math.floor((Math.random()*this.tresors.length));
	}
this.J3T2 = this.tresors[ct8];
ct9 = Math.floor((Math.random()*this.tresors.length));
while (ct9 == ct1 || ct9 == ct2 || ct9 == ct3 || ct9 == ct4 || ct9 == ct5 || ct9 == ct6 || ct9 == ct7 || ct9 == ct8){
	ct9 = Math.floor((Math.random()*this.tresors.length));
	}
this.J3T3 = this.tresors[ct9];
ct10 = Math.floor((Math.random()*this.tresors.length));
while (ct10 == ct1 || ct10 == ct2 || ct10 == ct3 || ct10 == ct4 || ct10 == ct5 || ct10 == ct6 || ct10 == ct7 || ct10 == ct8 || ct10 == ct9){
	ct10 = Math.floor((Math.random()*this.tresors.length));
	}
this.J4T1 = this.tresors[ct10];
ct11 = Math.floor((Math.random()*this.tresors.length));
while (ct11 == ct1 || ct11 == ct2 || ct11 == ct3 || ct11 == ct4 || ct11 == ct5 || ct11 == ct6 || ct11 == ct7 || ct11 == ct8 || ct11 == ct9 || ct11 == ct10){
	ct11 = Math.floor((Math.random()*this.tresors.length));
	}
this.J4T2 = this.tresors[ct11];
ct12 = Math.floor((Math.random()*this.tresors.length));
while (ct12 == ct1 || ct12 == ct2 || ct12 == ct3 || ct12 == ct4 || ct12 == ct5 || ct12 == ct6 || ct12 == ct7 || ct12 == ct8 || ct12 == ct9 || ct12 == ct10 || ct12 == ct11){
	ct12 = Math.floor((Math.random()*this.tresors.length));
	}
this.J4T3 = this.tresors[ct12];

}
Labyrinthe.prototype.masqueClique = false;
Labyrinthe.prototype.provenanceTuileEnTrop = { i : -1, j : -1 };

// Accesseur/mutateur de la case avec coordonnées (i, j) :
Labyrinthe.prototype.cell = function(i, j, c)
{
	 if(c==undefined)
   	 return this.cases[i*this.n + j];
	 this.cases[i*this.n + j] = c;
}
// Méthode qui affiche un labyrinthe dans la table HTML
// avec identifiant "labyrinth" et sa tuile en trop dans
// la table HTML avec indentifiant "tuile-en-trop" 
Labyrinthe.prototype.afficher = function()
{
	 // Fonction interne pour l'affichage d'une cellule :
	 function affCell(td, c, P)
	 {
	 	var t = c.getType();
  	   var img = document.createElement("img");
		img.setAttribute("src","./image/" + t + ".png");
		td.appendChild(img);
	 	}
	
	 table = document.getElementById("labyrinth");
	 table.setAttribute("cellpadding","0"); //reduit les espaces entre les cases
	 table.setAttribute("cellspacing","0"); //reduit les espaces entre les cases
	 table.innerHTML = ""; // Effaçons l'éventuel contenu précédent de la table
	 for(i = 0; i<this.n; i++)
	 {
	 	
	 	  tr = document.createElement("TR");
	 	  for(j = 0; j<this.n; j++)
	 	  {
	 	  	  td = document.createElement("TD");
	 	  	  td.appendChild(document.createTextNode(" ")); // sinon les cellules ne s'affichent pas correctement... 	  	  
	 	  	  td.setAttribute("onclick", "cliqueCase(" + i + ", " + j + ")");
	 	  	  // Pour les cases marginales des rangées impaires où on peut pousser la tuile en trop,
	 	  	  // il faut prédisposer les gesteurs des événements liés au glisser-déposer : 
	 	  	  if(((i == 0 || i == this.n - 1) && j%2 == 1) || ((j == 0 || j == this.n - 1) && i%2 == 1))
	 	  	  {
  	 	  	   td.setAttribute("ondragover", "allowDrop(event)");
	  	  	   td.setAttribute("ondrop", "drop(event, " + i + ", " + j + ")");
	  	   }
	 	  	  var c = this.cell(i, j);
	 	  	  // Dessiner les tresors :
if ((labyrinthe.tresor0.i == i) && (labyrinthe.tresor0.j == j)){
	       	var img = document.createElement("img");
	       	img.setAttribute("STYLE","margin-left:-20px");
	       	img.setAttribute("STYLE","position:absolute");
			 	img.setAttribute("src","./image/tresor0.png");
			 	td.appendChild(img);
          }
  else if ((labyrinthe.tresor1.i == i) && (labyrinthe.tresor1.j == j)){
	       	var img = document.createElement("img");
	       	img.setAttribute("STYLE","margin-left:-20px");
	       	img.setAttribute("STYLE","position:absolute");
			 	img.setAttribute("src","./image/tresor1.png");
			 	td.appendChild(img);
          }
  else if ((labyrinthe.tresor2.i == i) && (labyrinthe.tresor2.j == j)){
	       	var img = document.createElement("img");
	       	img.setAttribute("STYLE","margin-left:-20px");
	       	img.setAttribute("STYLE","position:absolute");
			 	img.setAttribute("src","./image/tresor2.png");
			 	td.appendChild(img);
          }
          else if ((labyrinthe.tresor3.i == i) && (labyrinthe.tresor3.j == j)){
	       	var img = document.createElement("img");
	       	img.setAttribute("STYLE","margin-left:-20px");
	       	img.setAttribute("STYLE","position:absolute");
			 	img.setAttribute("src","./image/tresor3.png");
			 	td.appendChild(img);
          }
          else if ((labyrinthe.tresor4.i == i) && (labyrinthe.tresor4.j == j)){
	       	var img = document.createElement("img");
	       	img.setAttribute("STYLE","margin-left:-20px");
	       	img.setAttribute("STYLE","position:absolute");
			 	img.setAttribute("src","./image/tresor4.png");
			 	td.appendChild(img);
          }
          else if ((labyrinthe.tresor5.i == i) && (labyrinthe.tresor5.j == j)){
	       	var img = document.createElement("img");
	       	img.setAttribute("STYLE","margin-left:-20px");
	       	img.setAttribute("STYLE","position:absolute");
			 	img.setAttribute("src","./image/tresor5.png");
			 	td.appendChild(img);
          }
          else if ((labyrinthe.tresor6.i == i) && (labyrinthe.tresor6.j == j)){
	       	var img = document.createElement("img");
	       	img.setAttribute("STYLE","margin-left:-20px");
	       	img.setAttribute("STYLE","position:absolute");
			 	img.setAttribute("src","./image/tresor6.png");
			 	td.appendChild(img);
          }
          else if ((labyrinthe.tresor7.i == i) && (labyrinthe.tresor7.j == j)){
	       	var img = document.createElement("img");
	       	img.setAttribute("STYLE","margin-left:-20px");
	       	img.setAttribute("STYLE","position:absolute");
			 	img.setAttribute("src","./image/tresor7.png");
			 	td.appendChild(img);
          }
          else if ((labyrinthe.tresor8.i == i) && (labyrinthe.tresor8.j == j)){
	       	var img = document.createElement("img");
	       	img.setAttribute("STYLE","margin-left:-20px");
	       	img.setAttribute("STYLE","position:absolute");
			 	img.setAttribute("src","./image/tresor8.png");
			 	td.appendChild(img);
          }
          else if ((labyrinthe.tresor9.i == i) && (labyrinthe.tresor9.j == j)){
	       	var img = document.createElement("img");
	       	img.setAttribute("STYLE","margin-left:-20px");
	       	img.setAttribute("STYLE","position:absolute");
			 	img.setAttribute("src","./image/tresor9.png");
			 	td.appendChild(img);
          }
          else if ((labyrinthe.tresor10.i == i) && (labyrinthe.tresor10.j == j)){
	       	var img = document.createElement("img");
	       	img.setAttribute("STYLE","margin-left:-20px");
	       	img.setAttribute("STYLE","position:absolute");
			 	img.setAttribute("src","./image/tresor10.png");
			 	td.appendChild(img);
          }
          else if ((labyrinthe.tresor11.i == i) && (labyrinthe.tresor11.j == j)){
	       	var img = document.createElement("img");
	       	img.setAttribute("STYLE","margin-left:-20px");
	       	img.setAttribute("STYLE","position:absolute");
			 	img.setAttribute("src","./image/tresor11.png");
			 	td.appendChild(img);
          }
          else if ((labyrinthe.tresor12.i == i) && (labyrinthe.tresor12.j == j)){
	       	var img = document.createElement("img");
	       	img.setAttribute("STYLE","margin-left:-20px");
	       	img.setAttribute("STYLE","position:absolute");
			 	img.setAttribute("src","./image/tresor12.png");
			 	td.appendChild(img);
          }
          else if ((labyrinthe.tresor13.i == i) && (labyrinthe.tresor13.j == j)){
	       	var img = document.createElement("img");
	       	img.setAttribute("STYLE","margin-left:-20px");
	       	img.setAttribute("STYLE","position:absolute");
			 	img.setAttribute("src","./image/tresor13.png");
			 	td.appendChild(img);
          }
          else if ((labyrinthe.tresor14.i == i) && (labyrinthe.tresor14.j == j)){
	       	var img = document.createElement("img");
	       	img.setAttribute("STYLE","margin-left:-20px");
	       	img.setAttribute("STYLE","position:absolute");
			 	img.setAttribute("src","./image/tresor14.png");
			 	td.appendChild(img);
          }
          else if ((labyrinthe.tresor15.i == i) && (labyrinthe.tresor15.j == j)){
	       	var img = document.createElement("img");
	       	img.setAttribute("STYLE","margin-left:-20px");
	       	img.setAttribute("STYLE","position:absolute");
			 	img.setAttribute("src","./image/tresor15.png");
			 	td.appendChild(img);
          }
          else if ((labyrinthe.tresor16.i == i) && (labyrinthe.tresor16.j == j)){
	       	var img = document.createElement("img");
	       	img.setAttribute("STYLE","margin-left:-20px");
	       	img.setAttribute("STYLE","position:absolute");
			 	img.setAttribute("src","./image/tresor16.png");
			 	td.appendChild(img);
          }
          // Dessiner les pions :
         if(this.nbrJ == 1){
          	if ((this.pionB.i == i) && (this.pionB.j == j)){
	       	var img = document.createElement("img");
	       	img.setAttribute("STYLE","margin-left:-20px");
	       	img.setAttribute("STYLE","position:absolute");
			 	img.setAttribute("src","./image/pionB.png");
			 	td.appendChild(img);
          }
          	}
         else if(this.nbrJ == 2){
          	if ((this.pionB.i == i) && (this.pionB.j == j)){
	       	var img = document.createElement("img");
	       	img.setAttribute("STYLE","margin-left:-20px");
	       	img.setAttribute("STYLE","position:absolute");
			 	img.setAttribute("src","./image/pionB.png");
			 	td.appendChild(img);
          } else if ((this.pionV.i == i) && (this.pionV.j == j)){
	       	var img = document.createElement("img");
	       	img.setAttribute("STYLE","margin-left:-20px");
	       	img.setAttribute("STYLE","position:absolute");
			 	img.setAttribute("src","./image/pionV.png");
			 	td.appendChild(img);
          }
          	}
         else if(this.nbrJ == 3){
          	if ((this.pionB.i == i) && (this.pionB.j == j)){
	       		var img = document.createElement("img");
	       		img.setAttribute("STYLE","margin-left:-20px");
	       		img.setAttribute("STYLE","position:absolute");
			 		img.setAttribute("src","./image/pionB.png");
			 		td.appendChild(img);
          	} else if ((this.pionR.i == i) && (this.pionR.j == j)){
	       		var img = document.createElement("img");
	       		img.setAttribute("STYLE","margin-left:-20px");
	       		img.setAttribute("STYLE","position:absolute");
			 		img.setAttribute("src","./image/pionR.png");
			 		td.appendChild(img);
          	} else if ((this.pionV.i == i) && (this.pionV.j == j)){
	       		var img = document.createElement("img");
	       		img.setAttribute("STYLE","margin-left:-20px");
	       		img.setAttribute("STYLE","position:absolute");
			 		img.setAttribute("src","./image/pionV.png");
			 		td.appendChild(img);
          }
          	}
         else if(this.nbrJ == 4){
          	if ((this.pionB.i == i) && (this.pionB.j == j)){
	       	var img = document.createElement("img");
	       	img.setAttribute("STYLE","margin-left:-20px");
	       	img.setAttribute("STYLE","position:absolute");
			 	img.setAttribute("src","./image/pionB.png");
			 	td.appendChild(img);
          } else if ((this.pionR.i == i) && (this.pionR.j == j)){
	       	var img = document.createElement("img");
	       	img.setAttribute("STYLE","margin-left:-20px");
	       	img.setAttribute("STYLE","position:absolute");
			 	img.setAttribute("src","./image/pionR.png");
			 	td.appendChild(img);
          } else if ((this.pionV.i == i) && (this.pionV.j == j)){
	       	var img = document.createElement("img");
	       	img.setAttribute("STYLE","margin-left:-20px");
	       	img.setAttribute("STYLE","position:absolute");
			 	img.setAttribute("src","./image/pionV.png");
			 	td.appendChild(img);
          } else if ((this.pionJ.i == i) && (this.pionJ.j == j)){
	       	var img = document.createElement("img");
	       	img.setAttribute("STYLE","margin-left:-20px");
	       	img.setAttribute("STYLE","position:absolute");
			 	img.setAttribute("src","./image/pionJ.png");
			 	td.appendChild(img);
          }
          	}
          affCell(td, c);
	 	  	  tr.appendChild(td);
	 	  	}
	 	  	table.appendChild(tr);
	 	}
	 	
	 	// Dessiner tuile en trop :
	 table = document.getElementById("tuile-en-trop");
	 table.innerHTML = ""; // Effaçons l'éventuel contenu précédent de la table
	 tr = document.createElement("TR");
	 td = document.createElement("TD");
	 td.appendChild(document.createTextNode(" ")); // sinon les cellules ne s'affichent pas correctement...
if ((labyrinthe.tresor0.i == -1) && (labyrinthe.tresor0.j == -1)){
	       	var img = document.createElement("img");
	       	img.setAttribute("STYLE","margin-left:-20px");
	       	img.setAttribute("STYLE","position:absolute");
			 	img.setAttribute("src","./image/tresor0.png");
			 	td.appendChild(img);
          }
  else if ((labyrinthe.tresor1.i == -1) && (labyrinthe.tresor1.j == -1)){
	       	var img = document.createElement("img");
	       	img.setAttribute("STYLE","margin-left:-20px");
	       	img.setAttribute("STYLE","position:absolute");
			 	img.setAttribute("src","./image/tresor1.png");
			 	td.appendChild(img);
          }
  else if ((labyrinthe.tresor2.i == -1) && (labyrinthe.tresor2.j == -1)){
	       	var img = document.createElement("img");
	       	img.setAttribute("STYLE","margin-left:-20px");
	       	img.setAttribute("STYLE","position:absolute");
			 	img.setAttribute("src","./image/tresor2.png");
			 	td.appendChild(img);
          }
          else if ((labyrinthe.tresor3.i == -1) && (labyrinthe.tresor3.j == -1)){
	       	var img = document.createElement("img");
	       	img.setAttribute("STYLE","margin-left:-20px");
	       	img.setAttribute("STYLE","position:absolute");
			 	img.setAttribute("src","./image/tresor3.png");
			 	td.appendChild(img);
          }
          else if ((labyrinthe.tresor4.i == -1) && (labyrinthe.tresor4.j == -1)){
	       	var img = document.createElement("img");
	       	img.setAttribute("STYLE","margin-left:-20px");
	       	img.setAttribute("STYLE","position:absolute");
			 	img.setAttribute("src","./image/tresor4.png");
			 	td.appendChild(img);
          }
          else if ((labyrinthe.tresor5.i == -1) && (labyrinthe.tresor5.j == -1)){
	       	var img = document.createElement("img");
	       	img.setAttribute("STYLE","margin-left:-20px");
	       	img.setAttribute("STYLE","position:absolute");
			 	img.setAttribute("src","./image/tresor5.png");
			 	td.appendChild(img);
          }
          else if ((labyrinthe.tresor6.i == -1) && (labyrinthe.tresor6.j == -1)){
	       	var img = document.createElement("img");
	       	img.setAttribute("STYLE","margin-left:-20px");
	       	img.setAttribute("STYLE","position:absolute");
			 	img.setAttribute("src","./image/tresor6.png");
			 	td.appendChild(img);
          }
          else if ((labyrinthe.tresor7.i == -1) && (labyrinthe.tresor7.j == -1)){
	       	var img = document.createElement("img");
	       	img.setAttribute("STYLE","margin-left:-20px");
	       	img.setAttribute("STYLE","position:absolute");
			 	img.setAttribute("src","./image/tresor7.png");
			 	td.appendChild(img);
          }
          else if ((labyrinthe.tresor8.i == -1) && (labyrinthe.tresor8.j == -1)){
	       	var img = document.createElement("img");
	       	img.setAttribute("STYLE","margin-left:-20px");
	       	img.setAttribute("STYLE","position:absolute");
			 	img.setAttribute("src","./image/tresor8.png");
			 	td.appendChild(img);
          }
          else if ((labyrinthe.tresor9.i == -1) && (labyrinthe.tresor9.j == -1)){
	       	var img = document.createElement("img");
	       	img.setAttribute("STYLE","margin-left:-20px");
	       	img.setAttribute("STYLE","position:absolute");
			 	img.setAttribute("src","./image/tresor9.png");
			 	td.appendChild(img);
          }
          else if ((labyrinthe.tresor10.i == -1) && (labyrinthe.tresor10.j == -1)){
	       	var img = document.createElement("img");
	       	img.setAttribute("STYLE","margin-left:-20px");
	       	img.setAttribute("STYLE","position:absolute");
			 	img.setAttribute("src","./image/tresor10.png");
			 	td.appendChild(img);
          }
          else if ((labyrinthe.tresor11.i == -1) && (labyrinthe.tresor11.j == -1)){
	       	var img = document.createElement("img");
	       	img.setAttribute("STYLE","margin-left:-20px");
	       	img.setAttribute("STYLE","position:absolute");
			 	img.setAttribute("src","./image/tresor11.png");
			 	td.appendChild(img);
          }
          else if ((labyrinthe.tresor12.i == -1) && (labyrinthe.tresor12.j == -1)){
	       	var img = document.createElement("img");
	       	img.setAttribute("STYLE","margin-left:-20px");
	       	img.setAttribute("STYLE","position:absolute");
			 	img.setAttribute("src","./image/tresor12.png");
			 	td.appendChild(img);
          }
          else if ((labyrinthe.tresor13.i == -1) && (labyrinthe.tresor13.j == -1)){
	       	var img = document.createElement("img");
	       	img.setAttribute("STYLE","margin-left:-20px");
	       	img.setAttribute("STYLE","position:absolute");
			 	img.setAttribute("src","./image/tresor13.png");
			 	td.appendChild(img);
          }
          else if ((labyrinthe.tresor14.i == -1) && (labyrinthe.tresor14.j == -1)){
	       	var img = document.createElement("img");
	       	img.setAttribute("STYLE","margin-left:-20px");
	       	img.setAttribute("STYLE","position:absolute");
			 	img.setAttribute("src","./image/tresor14.png");
			 	td.appendChild(img);
          }
          else if ((labyrinthe.tresor15.i == -1) && (labyrinthe.tresor15.j == -1)){
	       	var img = document.createElement("img");
	       	img.setAttribute("STYLE","margin-left:-20px");
	       	img.setAttribute("STYLE","position:absolute");
			 	img.setAttribute("src","./image/tresor15.png");
			 	td.appendChild(img);
          }
          else if ((labyrinthe.tresor16.i == -1) && (labyrinthe.tresor16.j == -1)){
	       	var img = document.createElement("img");
	       	img.setAttribute("STYLE","margin-left:-20px");
	       	img.setAttribute("STYLE","position:absolute");
			 	img.setAttribute("src","./image/tresor16.png");
			 	td.appendChild(img);
          }
     affCell(td, this.tuileEnTrop);
	 tr.appendChild(td);
	 table.appendChild(tr);
	 var jeux = document.getElementById("joueur");
	 var tabcarte = document.getElementById("carte");
  tabcarte.innerHTML = "";
	 var img200 = document.createElement("img");
	 img200.setAttribute("src","./image/carte1.png");
	 var img400 = document.createElement("img");
	 img400.setAttribute("src","./image/carte2.png");
	 var img600 = document.createElement("img");
	 img600.setAttribute("src","./image/carte3.png");
	  var img210 = document.createElement("img");
	 img210.setAttribute("src","./image/carte1.png");
	 var img410 = document.createElement("img");
	 img410.setAttribute("src","./image/carte2.png");
	 var img610 = document.createElement("img");
	 img610.setAttribute("src","./image/carte3.png");
	  var img220 = document.createElement("img");
	 img220.setAttribute("src","./image/carte1.png");
	 var img420 = document.createElement("img");
	 img420.setAttribute("src","./image/carte2.png");
	 var img620 = document.createElement("img");
	 img620.setAttribute("src","./image/carte3.png");
	  var img230 = document.createElement("img");
	 img230.setAttribute("src","./image/carte1.png");
	 var img430 = document.createElement("img");
	 img430.setAttribute("src","./image/carte2.png");
	 var img630 = document.createElement("img");
	 img630.setAttribute("src","./image/carte3.png");
	 var img1 = document.createElement("img");
	 img1.setAttribute("STYLE","margin-left:-20px");
	 img1.setAttribute("STYLE","position:absolute");
	 if(labyrinthe.J1T1.i == -2){
	 	img1.setAttribute("src","./image/carte.png");
	 	}
	 else if (labyrinthe.J1T1.i == labyrinthe.tresor0.i && labyrinthe.J1T1.j == labyrinthe.tresor0.j){
	 img1.setAttribute("src","./image/ctresor0.png");
	}
	else if (labyrinthe.J1T1.i == labyrinthe.tresor1.i && labyrinthe.J1T1.j == labyrinthe.tresor1.j){
	 img1.setAttribute("src","./image/ctresor1.png");
	}
	else if (labyrinthe.J1T1.i == labyrinthe.tresor2.i && labyrinthe.J1T1.j == labyrinthe.tresor2.j){
	 img1.setAttribute("src","./image/ctresor2.png");
	}
	else if (labyrinthe.J1T1.i == labyrinthe.tresor3.i && labyrinthe.J1T1.j == labyrinthe.tresor3.j){
	 img1.setAttribute("src","./image/ctresor3.png");
	}
	else if (labyrinthe.J1T1.i == labyrinthe.tresor4.i && labyrinthe.J1T1.j == labyrinthe.tresor4.j){
	 img1.setAttribute("src","./image/ctresor4.png");
	}
	else if (labyrinthe.J1T1.i == labyrinthe.tresor5.i && labyrinthe.J1T1.j == labyrinthe.tresor5.j){
	 img1.setAttribute("src","./image/ctresor5.png");
	}
	else if (labyrinthe.J1T1.i == labyrinthe.tresor6.i && labyrinthe.J1T1.j == labyrinthe.tresor6.j){
	 img1.setAttribute("src","./image/ctresor6.png");
	}
	else if (labyrinthe.J1T1.i == labyrinthe.tresor7.i && labyrinthe.J1T1.j == labyrinthe.tresor7.j){
	 img1.setAttribute("src","./image/ctresor7.png");
	}
	else if (labyrinthe.J1T1.i == labyrinthe.tresor8.i && labyrinthe.J1T1.j == labyrinthe.tresor8.j){
	 img1.setAttribute("src","./image/ctresor8.png");
	}
	else if (labyrinthe.J1T1.i == labyrinthe.tresor9.i && labyrinthe.J1T1.j == labyrinthe.tresor9.j){
	 img1.setAttribute("src","./image/ctresor9.png");
	}
	else if (labyrinthe.J1T1.i == labyrinthe.tresor10.i && labyrinthe.J1T1.j == labyrinthe.tresor10.j){
	 img1.setAttribute("src","./image/ctresor10.png");
	}
	else if (labyrinthe.J1T1.i == labyrinthe.tresor11.i && labyrinthe.J1T1.j == labyrinthe.tresor11.j){
	 img1.setAttribute("src","./image/ctresor11.png");
	}
	else if (labyrinthe.J1T1.i == labyrinthe.tresor12.i && labyrinthe.J1T1.j == labyrinthe.tresor12.j){
	 img1.setAttribute("src","./image/ctresor12.png");
	}
	else if (labyrinthe.J1T1.i == labyrinthe.tresor13.i && labyrinthe.J1T1.j == labyrinthe.tresor13.j){
	 img1.setAttribute("src","./image/ctresor13.png");
	}
	else if (labyrinthe.J1T1.i == labyrinthe.tresor14.i && labyrinthe.J1T1.j == labyrinthe.tresor14.j){
	 img1.setAttribute("src","./image/ctresor14.png");
	}
	else if (labyrinthe.J1T1.i == labyrinthe.tresor15.i && labyrinthe.J1T1.j == labyrinthe.tresor15.j){
	 img1.setAttribute("src","./image/ctresor15.png");
	}
	else if (labyrinthe.J1T1.i == labyrinthe.tresor16.i && labyrinthe.J1T1.j == labyrinthe.tresor16.j){
	 img1.setAttribute("src","./image/ctresor16.png");
	}
	
	 var img3 = document.createElement("img");
	 img3.setAttribute("STYLE","margin-left:-20px");
	 img3.setAttribute("STYLE","position:absolute");
	if(labyrinthe.J1T2.i == -2){
	 	img3.setAttribute("src","./image/carte.png");
	 	}
	 	 else if (labyrinthe.J1T2.i == labyrinthe.tresor0.i && labyrinthe.J1T2.j == labyrinthe.tresor0.j){
	 img3.setAttribute("src","./image/ctresor0.png");
	}
	else if (labyrinthe.J1T2.i == labyrinthe.tresor1.i && labyrinthe.J1T2.j == labyrinthe.tresor1.j){
	 img3.setAttribute("src","./image/ctresor1.png");
	}
	else if (labyrinthe.J1T2.i == labyrinthe.tresor2.i && labyrinthe.J1T2.j == labyrinthe.tresor2.j){
	 img3.setAttribute("src","./image/ctresor2.png");
	}
	else if (labyrinthe.J1T2.i == labyrinthe.tresor3.i && labyrinthe.J1T2.j == labyrinthe.tresor3.j){
	 img3.setAttribute("src","./image/ctresor3.png");
	}
	else if (labyrinthe.J1T2.i == labyrinthe.tresor4.i && labyrinthe.J1T2.j == labyrinthe.tresor4.j){
	 img3.setAttribute("src","./image/ctresor4.png");
	}
	else if (labyrinthe.J1T2.i == labyrinthe.tresor5.i && labyrinthe.J1T2.j == labyrinthe.tresor5.j){
	 img3.setAttribute("src","./image/ctresor5.png");
	}
	else if (labyrinthe.J1T2.i == labyrinthe.tresor6.i && labyrinthe.J1T2.j == labyrinthe.tresor6.j){
	 img3.setAttribute("src","./image/ctresor6.png");
	}
	else if (labyrinthe.J1T2.i == labyrinthe.tresor7.i && labyrinthe.J1T2.j == labyrinthe.tresor7.j){
	 img3.setAttribute("src","./image/ctresor7.png");
	}
	else if (labyrinthe.J1T2.i == labyrinthe.tresor8.i && labyrinthe.J1T2.j == labyrinthe.tresor8.j){
	 img3.setAttribute("src","./image/ctresor8.png");
	}
	else if (labyrinthe.J1T2.i == labyrinthe.tresor9.i && labyrinthe.J1T2.j == labyrinthe.tresor9.j){
	 img3.setAttribute("src","./image/ctresor9.png");
	}
	else if (labyrinthe.J1T2.i == labyrinthe.tresor10.i && labyrinthe.J1T2.j == labyrinthe.tresor10.j){
	 img3.setAttribute("src","./image/ctresor10.png");
	}
	else if (labyrinthe.J1T2.i == labyrinthe.tresor11.i && labyrinthe.J1T2.j == labyrinthe.tresor11.j){
	 img3.setAttribute("src","./image/ctresor11.png");
	}
	else if (labyrinthe.J1T2.i == labyrinthe.tresor12.i && labyrinthe.J1T2.j == labyrinthe.tresor12.j){
	 img3.setAttribute("src","./image/ctresor12.png");
	}
	else if (labyrinthe.J1T2.i == labyrinthe.tresor13.i && labyrinthe.J1T2.j == labyrinthe.tresor13.j){
	 img3.setAttribute("src","./image/ctresor13.png");
	}
	else if (labyrinthe.J1T2.i == labyrinthe.tresor14.i && labyrinthe.J1T2.j == labyrinthe.tresor14.j){
	 img3.setAttribute("src","./image/ctresor14.png");
	}
	else if (labyrinthe.J1T2.i == labyrinthe.tresor15.i && labyrinthe.J1T2.j == labyrinthe.tresor15.j){
	 img3.setAttribute("src","./image/ctresor15.png");
	}
	else if (labyrinthe.J1T2.i == labyrinthe.tresor16.i && labyrinthe.J1T2.j == labyrinthe.tresor16.j){
	 img3.setAttribute("src","./image/ctresor16.png");
	}
	 var img5 = document.createElement("img");
	 img5.setAttribute("STYLE","margin-left:-20px");
	 img5.setAttribute("STYLE","position:absolute");
	if(labyrinthe.J1T3.i == -2){
	 	img5.setAttribute("src","./image/carte.png");
	 	}
	 else if (labyrinthe.J1T3.i == labyrinthe.tresor0.i && labyrinthe.J1T3.j == labyrinthe.tresor0.j){
	 img5.setAttribute("src","./image/ctresor0.png");
	}
	else if (labyrinthe.J1T3.i == labyrinthe.tresor1.i && labyrinthe.J1T3.j == labyrinthe.tresor1.j){
	 img5.setAttribute("src","./image/ctresor1.png");
	}
	else if (labyrinthe.J1T3.i == labyrinthe.tresor2.i && labyrinthe.J1T3.j == labyrinthe.tresor2.j){
	 img5.setAttribute("src","./image/ctresor2.png");
	}
	else if (labyrinthe.J1T3.i == labyrinthe.tresor3.i && labyrinthe.J1T3.j == labyrinthe.tresor3.j){
	 img5.setAttribute("src","./image/ctresor3.png");
	}
	else if (labyrinthe.J1T3.i == labyrinthe.tresor4.i && labyrinthe.J1T3.j == labyrinthe.tresor4.j){
	 img5.setAttribute("src","./image/ctresor4.png");
	}
	else if (labyrinthe.J1T3.i == labyrinthe.tresor5.i && labyrinthe.J1T3.j == labyrinthe.tresor5.j){
	 img5.setAttribute("src","./image/ctresor5.png");
	}
	else if (labyrinthe.J1T3.i == labyrinthe.tresor6.i && labyrinthe.J1T3.j == labyrinthe.tresor6.j){
	 img5.setAttribute("src","./image/ctresor6.png");
	}
	else if (labyrinthe.J1T3.i == labyrinthe.tresor7.i && labyrinthe.J1T3.j == labyrinthe.tresor7.j){
	 img5.setAttribute("src","./image/ctresor7.png");
	}
	else if (labyrinthe.J1T3.i == labyrinthe.tresor8.i && labyrinthe.J1T3.j == labyrinthe.tresor8.j){
	 img5.setAttribute("src","./image/ctresor8.png");
	}
	else if (labyrinthe.J1T3.i == labyrinthe.tresor9.i && labyrinthe.J1T3.j == labyrinthe.tresor9.j){
	 img5.setAttribute("src","./image/ctresor9.png");
	}
	else if (labyrinthe.J1T3.i == labyrinthe.tresor10.i && labyrinthe.J1T3.j == labyrinthe.tresor10.j){
	 img5.setAttribute("src","./image/ctresor10.png");
	}
	else if (labyrinthe.J1T3.i == labyrinthe.tresor11.i && labyrinthe.J1T3.j == labyrinthe.tresor11.j){
	 img5.setAttribute("src","./image/ctresor11.png");
	}
	else if (labyrinthe.J1T3.i == labyrinthe.tresor12.i && labyrinthe.J1T3.j == labyrinthe.tresor12.j){
	 img5.setAttribute("src","./image/ctresor12.png");
	}
	else if (labyrinthe.J1T3.i == labyrinthe.tresor13.i && labyrinthe.J1T3.j == labyrinthe.tresor13.j){
	 img5.setAttribute("src","./image/ctresor13.png");
	}
	else if (labyrinthe.J1T3.i == labyrinthe.tresor14.i && labyrinthe.J1T3.j == labyrinthe.tresor14.j){
	 img5.setAttribute("src","./image/ctresor14.png");
	}
	else if (labyrinthe.J1T3.i == labyrinthe.tresor15.i && labyrinthe.J1T3.j == labyrinthe.tresor15.j){
	 img5.setAttribute("src","./image/ctresor15.png");
	}
	else if (labyrinthe.J1T3.i == labyrinthe.tresor16.i && labyrinthe.J1T3.j == labyrinthe.tresor16.j){
	 img5.setAttribute("src","./image/ctresor16.png");
	}
	 var img7 = document.createElement("img");
	 img7.setAttribute("STYLE","margin-left:-20px");
	 img7.setAttribute("STYLE","position:absolute");
	if(labyrinthe.J2T1.i == -2){
	 	img7.setAttribute("src","./image/carte.png");
	 	}
	 	 else if (labyrinthe.J2T1.i == labyrinthe.tresor0.i && labyrinthe.J2T1.j == labyrinthe.tresor0.j){
	 img7.setAttribute("src","./image/ctresor0.png");
	}
	else if (labyrinthe.J2T1.i == labyrinthe.tresor1.i && labyrinthe.J2T1.j == labyrinthe.tresor1.j){
	 img7.setAttribute("src","./image/ctresor1.png");
	}
	else if (labyrinthe.J2T1.i == labyrinthe.tresor2.i && labyrinthe.J2T1.j == labyrinthe.tresor2.j){
	 img7.setAttribute("src","./image/ctresor2.png");
	}
	else if (labyrinthe.J2T1.i == labyrinthe.tresor3.i && labyrinthe.J2T1.j == labyrinthe.tresor3.j){
	 img7.setAttribute("src","./image/ctresor3.png");
	}
	else if (labyrinthe.J2T1.i == labyrinthe.tresor4.i && labyrinthe.J2T1.j == labyrinthe.tresor4.j){
	 img7.setAttribute("src","./image/ctresor4.png");
	}
	else if (labyrinthe.J2T1.i == labyrinthe.tresor5.i && labyrinthe.J2T1.j == labyrinthe.tresor5.j){
	 img7.setAttribute("src","./image/ctresor5.png");
	}
	else if (labyrinthe.J2T1.i == labyrinthe.tresor6.i && labyrinthe.J2T1.j == labyrinthe.tresor6.j){
	 img7.setAttribute("src","./image/ctresor6.png");
	}
	else if (labyrinthe.J2T1.i == labyrinthe.tresor7.i && labyrinthe.J2T1.j == labyrinthe.tresor7.j){
	 img7.setAttribute("src","./image/ctresor7.png");
	}
	else if (labyrinthe.J2T1.i == labyrinthe.tresor8.i && labyrinthe.J2T1.j == labyrinthe.tresor8.j){
	 img7.setAttribute("src","./image/ctresor8.png");
	}
	else if (labyrinthe.J2T1.i == labyrinthe.tresor9.i && labyrinthe.J2T1.j == labyrinthe.tresor9.j){
	 img7.setAttribute("src","./image/ctresor9.png");
	}
	else if (labyrinthe.J2T1.i == labyrinthe.tresor10.i && labyrinthe.J2T1.j == labyrinthe.tresor10.j){
	 img7.setAttribute("src","./image/ctresor10.png");
	}
	else if (labyrinthe.J2T1.i == labyrinthe.tresor11.i && labyrinthe.J2T1.j == labyrinthe.tresor11.j){
	 img7.setAttribute("src","./image/ctresor11.png");
	}
	else if (labyrinthe.J2T1.i == labyrinthe.tresor12.i && labyrinthe.J2T1.j == labyrinthe.tresor12.j){
	 img7.setAttribute("src","./image/ctresor12.png");
	}
	else if (labyrinthe.J2T1.i == labyrinthe.tresor13.i && labyrinthe.J2T1.j == labyrinthe.tresor13.j){
	 img7.setAttribute("src","./image/ctresor13.png");
	}
	else if (labyrinthe.J2T1.i == labyrinthe.tresor14.i && labyrinthe.J2T1.j == labyrinthe.tresor14.j){
	 img7.setAttribute("src","./image/ctresor14.png");
	}
	else if (labyrinthe.J2T1.i == labyrinthe.tresor15.i && labyrinthe.J2T1.j == labyrinthe.tresor15.j){
	 img7.setAttribute("src","./image/ctresor15.png");
	}
	else if (labyrinthe.J2T1.i == labyrinthe.tresor16.i && labyrinthe.J2T1.j == labyrinthe.tresor16.j){
	 img7.setAttribute("src","./image/ctresor16.png");
	}
	 var img9 = document.createElement("img");
	 img9.setAttribute("STYLE","margin-left:-20px");
	 img9.setAttribute("STYLE","position:absolute");
	if(labyrinthe.J2T2.i == -2){
	 	img9.setAttribute("src","./image/carte.png");
	 	}
	 	 else if (labyrinthe.J2T2.i == labyrinthe.tresor0.i && labyrinthe.J2T2.j == labyrinthe.tresor0.j){
	 img9.setAttribute("src","./image/ctresor0.png");
	}
	else if (labyrinthe.J2T2.i == labyrinthe.tresor1.i && labyrinthe.J2T2.j == labyrinthe.tresor1.j){
	 img9.setAttribute("src","./image/ctresor1.png");
	}
	else if (labyrinthe.J2T2.i == labyrinthe.tresor2.i && labyrinthe.J2T2.j == labyrinthe.tresor2.j){
	 img9.setAttribute("src","./image/ctresor2.png");
	}
	else if (labyrinthe.J2T2.i == labyrinthe.tresor3.i && labyrinthe.J2T2.j == labyrinthe.tresor3.j){
	 img9.setAttribute("src","./image/ctresor3.png");
	}
	else if (labyrinthe.J2T2.i == labyrinthe.tresor4.i && labyrinthe.J2T2.j == labyrinthe.tresor4.j){
	 img9.setAttribute("src","./image/ctresor4.png");
	}
	else if (labyrinthe.J2T2.i == labyrinthe.tresor5.i && labyrinthe.J2T2.j == labyrinthe.tresor5.j){
	 img9.setAttribute("src","./image/ctresor5.png");
	}
	else if (labyrinthe.J2T2.i == labyrinthe.tresor6.i && labyrinthe.J2T2.j == labyrinthe.tresor6.j){
	 img9.setAttribute("src","./image/ctresor6.png");
	}
	else if (labyrinthe.J2T2.i == labyrinthe.tresor7.i && labyrinthe.J2T2.j == labyrinthe.tresor7.j){
	 img9.setAttribute("src","./image/ctresor7.png");
	}
	else if (labyrinthe.J2T2.i == labyrinthe.tresor8.i && labyrinthe.J2T2.j == labyrinthe.tresor8.j){
	 img9.setAttribute("src","./image/ctresor8.png");
	}
	else if (labyrinthe.J2T2.i == labyrinthe.tresor9.i && labyrinthe.J2T2.j == labyrinthe.tresor9.j){
	 img9.setAttribute("src","./image/ctresor9.png");
	}
	else if (labyrinthe.J2T2.i == labyrinthe.tresor10.i && labyrinthe.J2T2.j == labyrinthe.tresor10.j){
	 img9.setAttribute("src","./image/ctresor10.png");
	}
	else if (labyrinthe.J2T2.i == labyrinthe.tresor11.i && labyrinthe.J2T2.j == labyrinthe.tresor11.j){
	 img9.setAttribute("src","./image/ctresor11.png");
	}
	else if (labyrinthe.J2T2.i == labyrinthe.tresor12.i && labyrinthe.J2T2.j == labyrinthe.tresor12.j){
	 img9.setAttribute("src","./image/ctresor12.png");
	}
	else if (labyrinthe.J2T2.i == labyrinthe.tresor13.i && labyrinthe.J2T2.j == labyrinthe.tresor13.j){
	 img9.setAttribute("src","./image/ctresor13.png");
	}
	else if (labyrinthe.J2T2.i == labyrinthe.tresor14.i && labyrinthe.J2T2.j == labyrinthe.tresor14.j){
	 img9.setAttribute("src","./image/ctresor14.png");
	}
	else if (labyrinthe.J2T2.i == labyrinthe.tresor15.i && labyrinthe.J2T2.j == labyrinthe.tresor15.j){
	 img9.setAttribute("src","./image/ctresor15.png");
	}
	else if (labyrinthe.J2T2.i == labyrinthe.tresor16.i && labyrinthe.J2T2.j == labyrinthe.tresor16.j){
	 img9.setAttribute("src","./image/ctresor16.png");
	}
	 var img11 = document.createElement("img");
	 img11.setAttribute("STYLE","margin-left:-20px");
	 img11.setAttribute("STYLE","position:absolute");
	if(labyrinthe.J2T3.i == -2){
	 	img11.setAttribute("src","./image/carte.png");
	 	}
	 	 else if (labyrinthe.J2T3.i == labyrinthe.tresor0.i && labyrinthe.J2T3.j == labyrinthe.tresor0.j){
	 img11.setAttribute("src","./image/ctresor0.png");
	}
	else if (labyrinthe.J2T3.i == labyrinthe.tresor1.i && labyrinthe.J2T3.j == labyrinthe.tresor1.j){
	 img11.setAttribute("src","./image/ctresor1.png");
	}
	else if (labyrinthe.J2T3.i == labyrinthe.tresor2.i && labyrinthe.J2T3.j == labyrinthe.tresor2.j){
	 img11.setAttribute("src","./image/ctresor2.png");
	}
	else if (labyrinthe.J2T3.i == labyrinthe.tresor3.i && labyrinthe.J2T3.j == labyrinthe.tresor3.j){
	 img11.setAttribute("src","./image/ctresor3.png");
	}
	else if (labyrinthe.J2T3.i == labyrinthe.tresor4.i && labyrinthe.J2T3.j == labyrinthe.tresor4.j){
	 img11.setAttribute("src","./image/ctresor4.png");
	}
	else if (labyrinthe.J2T3.i == labyrinthe.tresor5.i && labyrinthe.J2T3.j == labyrinthe.tresor5.j){
	 img11.setAttribute("src","./image/ctresor5.png");
	}
	else if (labyrinthe.J2T3.i == labyrinthe.tresor6.i && labyrinthe.J2T3.j == labyrinthe.tresor6.j){
	 img11.setAttribute("src","./image/ctresor6.png");
	}
	else if (labyrinthe.J2T3.i == labyrinthe.tresor7.i && labyrinthe.J2T3.j == labyrinthe.tresor7.j){
	 img11.setAttribute("src","./image/ctresor7.png");
	}
	else if (labyrinthe.J2T3.i == labyrinthe.tresor8.i && labyrinthe.J2T3.j == labyrinthe.tresor8.j){
	 img11.setAttribute("src","./image/ctresor8.png");
	}
	else if (labyrinthe.J2T3.i == labyrinthe.tresor9.i && labyrinthe.J2T3.j == labyrinthe.tresor9.j){
	 img11.setAttribute("src","./image/ctresor9.png");
	}
	else if (labyrinthe.J2T3.i == labyrinthe.tresor10.i && labyrinthe.J2T3.j == labyrinthe.tresor10.j){
	 img11.setAttribute("src","./image/ctresor10.png");
	}
	else if (labyrinthe.J2T3.i == labyrinthe.tresor11.i && labyrinthe.J2T3.j == labyrinthe.tresor11.j){
	 img11.setAttribute("src","./image/ctresor11.png");
	}
	else if (labyrinthe.J2T3.i == labyrinthe.tresor12.i && labyrinthe.J2T3.j == labyrinthe.tresor12.j){
	 img11.setAttribute("src","./image/ctresor12.png");
	}
	else if (labyrinthe.J2T3.i == labyrinthe.tresor13.i && labyrinthe.J2T3.j == labyrinthe.tresor13.j){
	 img11.setAttribute("src","./image/ctresor13.png");
	}
	else if (labyrinthe.J2T3.i == labyrinthe.tresor14.i && labyrinthe.J2T3.j == labyrinthe.tresor14.j){
	 img11.setAttribute("src","./image/ctresor14.png");
	}
	else if (labyrinthe.J2T3.i == labyrinthe.tresor15.i && labyrinthe.J2T3.j == labyrinthe.tresor15.j){
	 img11.setAttribute("src","./image/ctresor15.png");
	}
	else if (labyrinthe.J2T3.i == labyrinthe.tresor16.i && labyrinthe.J2T3.j == labyrinthe.tresor16.j){
	 img11.setAttribute("src","./image/ctresor16.png");
	}
	 var img13 = document.createElement("img");
	 img13.setAttribute("STYLE","margin-left:-20px");
	 img13.setAttribute("STYLE","position:absolute");
	if(labyrinthe.J3T1.i == -2){
	 	img13.setAttribute("src","./image/carte.png");
	 	}
	 	 else if (labyrinthe.J3T1.i == labyrinthe.tresor0.i && labyrinthe.J3T1.j == labyrinthe.tresor0.j){
	 img13.setAttribute("src","./image/ctresor0.png");
	}
	else if (labyrinthe.J3T1.i == labyrinthe.tresor1.i && labyrinthe.J3T1.j == labyrinthe.tresor1.j){
	 img13.setAttribute("src","./image/ctresor1.png");
	}
	else if (labyrinthe.J3T1.i == labyrinthe.tresor2.i && labyrinthe.J3T1.j == labyrinthe.tresor2.j){
	 img13.setAttribute("src","./image/ctresor2.png");
	}
	else if (labyrinthe.J3T1.i == labyrinthe.tresor3.i && labyrinthe.J3T1.j == labyrinthe.tresor3.j){
	 img13.setAttribute("src","./image/ctresor3.png");
	}
	else if (labyrinthe.J3T1.i == labyrinthe.tresor4.i && labyrinthe.J3T1.j == labyrinthe.tresor4.j){
	 img13.setAttribute("src","./image/ctresor4.png");
	}
	else if (labyrinthe.J3T1.i == labyrinthe.tresor5.i && labyrinthe.J3T1.j == labyrinthe.tresor5.j){
	 img13.setAttribute("src","./image/ctresor5.png");
	}
	else if (labyrinthe.J3T1.i == labyrinthe.tresor6.i && labyrinthe.J3T1.j == labyrinthe.tresor6.j){
	 img13.setAttribute("src","./image/ctresor6.png");
	}
	else if (labyrinthe.J3T1.i == labyrinthe.tresor7.i && labyrinthe.J3T1.j == labyrinthe.tresor7.j){
	 img13.setAttribute("src","./image/ctresor7.png");
	}
	else if (labyrinthe.J3T1.i == labyrinthe.tresor8.i && labyrinthe.J3T1.j == labyrinthe.tresor8.j){
	 img13.setAttribute("src","./image/ctresor8.png");
	}
	else if (labyrinthe.J3T1.i == labyrinthe.tresor9.i && labyrinthe.J3T1.j == labyrinthe.tresor9.j){
	 img13.setAttribute("src","./image/ctresor9.png");
	}
	else if (labyrinthe.J3T1.i == labyrinthe.tresor10.i && labyrinthe.J3T1.j == labyrinthe.tresor10.j){
	 img13.setAttribute("src","./image/ctresor10.png");
	}
	else if (labyrinthe.J3T1.i == labyrinthe.tresor11.i && labyrinthe.J3T1.j == labyrinthe.tresor11.j){
	 img13.setAttribute("src","./image/ctresor11.png");
	}
	else if (labyrinthe.J3T1.i == labyrinthe.tresor12.i && labyrinthe.J3T1.j == labyrinthe.tresor12.j){
	 img13.setAttribute("src","./image/ctresor12.png");
	}
	else if (labyrinthe.J3T1.i == labyrinthe.tresor13.i && labyrinthe.J3T1.j == labyrinthe.tresor13.j){
	 img13.setAttribute("src","./image/ctresor13.png");
	}
	else if (labyrinthe.J3T1.i == labyrinthe.tresor14.i && labyrinthe.J3T1.j == labyrinthe.tresor14.j){
	 img13.setAttribute("src","./image/ctresor14.png");
	}
	else if (labyrinthe.J3T1.i == labyrinthe.tresor15.i && labyrinthe.J3T1.j == labyrinthe.tresor15.j){
	 img13.setAttribute("src","./image/ctresor15.png");
	}
	else if (labyrinthe.J3T1.i == labyrinthe.tresor16.i && labyrinthe.J3T1.j == labyrinthe.tresor16.j){
	 img13.setAttribute("src","./image/ctresor16.png");
	}
	 var img17 = document.createElement("img");
	 img17.setAttribute("STYLE","margin-left:-20px");
	 img17.setAttribute("STYLE","position:absolute");
	if(labyrinthe.J3T2.i == -2){
	 	img17.setAttribute("src","./image/carte.png");
	 	}
	 	 else if (labyrinthe.J3T2.i == labyrinthe.tresor0.i && labyrinthe.J3T2.j == labyrinthe.tresor0.j){
	 img17.setAttribute("src","./image/ctresor0.png");
	}
	else if (labyrinthe.J3T2.i == labyrinthe.tresor1.i && labyrinthe.J3T2.j == labyrinthe.tresor1.j){
	 img17.setAttribute("src","./image/ctresor1.png");
	}
	else if (labyrinthe.J3T2.i == labyrinthe.tresor2.i && labyrinthe.J3T2.j == labyrinthe.tresor2.j){
	 img17.setAttribute("src","./image/ctresor2.png");
	}
	else if (labyrinthe.J3T2.i == labyrinthe.tresor3.i && labyrinthe.J3T2.j == labyrinthe.tresor3.j){
	 img17.setAttribute("src","./image/ctresor3.png");
	}
	else if (labyrinthe.J3T2.i == labyrinthe.tresor4.i && labyrinthe.J3T2.j == labyrinthe.tresor4.j){
	 img17.setAttribute("src","./image/ctresor4.png");
	}
	else if (labyrinthe.J3T2.i == labyrinthe.tresor5.i && labyrinthe.J3T2.j == labyrinthe.tresor5.j){
	 img17.setAttribute("src","./image/ctresor5.png");
	}
	else if (labyrinthe.J3T2.i == labyrinthe.tresor6.i && labyrinthe.J3T2.j == labyrinthe.tresor6.j){
	 img17.setAttribute("src","./image/ctresor6.png");
	}
	else if (labyrinthe.J3T2.i == labyrinthe.tresor7.i && labyrinthe.J3T2.j == labyrinthe.tresor7.j){
	 img17.setAttribute("src","./image/ctresor7.png");
	}
	else if (labyrinthe.J3T2.i == labyrinthe.tresor8.i && labyrinthe.J3T2.j == labyrinthe.tresor8.j){
	 img17.setAttribute("src","./image/ctresor8.png");
	}
	else if (labyrinthe.J3T2.i == labyrinthe.tresor9.i && labyrinthe.J3T2.j == labyrinthe.tresor9.j){
	 img17.setAttribute("src","./image/ctresor9.png");
	}
	else if (labyrinthe.J3T2.i == labyrinthe.tresor10.i && labyrinthe.J3T2.j == labyrinthe.tresor10.j){
	 img17.setAttribute("src","./image/ctresor10.png");
	}
	else if (labyrinthe.J3T2.i == labyrinthe.tresor11.i && labyrinthe.J3T2.j == labyrinthe.tresor11.j){
	 img17.setAttribute("src","./image/ctresor11.png");
	}
	else if (labyrinthe.J3T2.i == labyrinthe.tresor12.i && labyrinthe.J3T2.j == labyrinthe.tresor12.j){
	 img17.setAttribute("src","./image/ctresor12.png");
	}
	else if (labyrinthe.J3T2.i == labyrinthe.tresor13.i && labyrinthe.J3T2.j == labyrinthe.tresor13.j){
	 img17.setAttribute("src","./image/ctresor13.png");
	}
	else if (labyrinthe.J3T2.i == labyrinthe.tresor14.i && labyrinthe.J3T2.j == labyrinthe.tresor14.j){
	 img17.setAttribute("src","./image/ctresor14.png");
	}
	else if (labyrinthe.J3T2.i == labyrinthe.tresor15.i && labyrinthe.J3T2.j == labyrinthe.tresor15.j){
	 img17.setAttribute("src","./image/ctresor15.png");
	}
	else if (labyrinthe.J3T2.i == labyrinthe.tresor16.i && labyrinthe.J3T2.j == labyrinthe.tresor16.j){
	 img17.setAttribute("src","./image/ctresor16.png");
	}
	 var img19 = document.createElement("img");
	 img19.setAttribute("STYLE","margin-left:-20px");
	 img19.setAttribute("STYLE","position:absolute");
	if(labyrinthe.J3T3.i == -2){
	 	img19.setAttribute("src","./image/carte.png");
	 	}
	 	 else if (labyrinthe.J3T3.i == labyrinthe.tresor0.i && labyrinthe.J3T3.j == labyrinthe.tresor0.j){
	 img19.setAttribute("src","./image/ctresor0.png");
	}
	else if (labyrinthe.J3T3.i == labyrinthe.tresor1.i && labyrinthe.J3T3.j == labyrinthe.tresor1.j){
	 img19.setAttribute("src","./image/ctresor1.png");
	}
	else if (labyrinthe.J3T3.i == labyrinthe.tresor2.i && labyrinthe.J3T3.j == labyrinthe.tresor2.j){
	 img19.setAttribute("src","./image/ctresor2.png");
	}
	else if (labyrinthe.J3T3.i == labyrinthe.tresor3.i && labyrinthe.J3T3.j == labyrinthe.tresor3.j){
	 img19.setAttribute("src","./image/ctresor3.png");
	}
	else if (labyrinthe.J3T3.i == labyrinthe.tresor4.i && labyrinthe.J3T3.j == labyrinthe.tresor4.j){
	 img19.setAttribute("src","./image/ctresor4.png");
	}
	else if (labyrinthe.J3T3.i == labyrinthe.tresor5.i && labyrinthe.J3T3.j == labyrinthe.tresor5.j){
	 img19.setAttribute("src","./image/ctresor5.png");
	}
	else if (labyrinthe.J3T3.i == labyrinthe.tresor6.i && labyrinthe.J3T3.j == labyrinthe.tresor6.j){
	 img19.setAttribute("src","./image/ctresor6.png");
	}
	else if (labyrinthe.J3T3.i == labyrinthe.tresor7.i && labyrinthe.J3T3.j == labyrinthe.tresor7.j){
	 img19.setAttribute("src","./image/ctresor7.png");
	}
	else if (labyrinthe.J3T3.i == labyrinthe.tresor8.i && labyrinthe.J3T3.j == labyrinthe.tresor8.j){
	 img19.setAttribute("src","./image/ctresor8.png");
	}
	else if (labyrinthe.J3T3.i == labyrinthe.tresor9.i && labyrinthe.J3T3.j == labyrinthe.tresor9.j){
	 img19.setAttribute("src","./image/ctresor9.png");
	}
	else if (labyrinthe.J3T3.i == labyrinthe.tresor10.i && labyrinthe.J3T3.j == labyrinthe.tresor10.j){
	 img19.setAttribute("src","./image/ctresor10.png");
	}
	else if (labyrinthe.J3T3.i == labyrinthe.tresor11.i && labyrinthe.J3T3.j == labyrinthe.tresor11.j){
	 img19.setAttribute("src","./image/ctresor11.png");
	}
	else if (labyrinthe.J3T3.i == labyrinthe.tresor12.i && labyrinthe.J3T3.j == labyrinthe.tresor12.j){
	 img19.setAttribute("src","./image/ctresor12.png");
	}
	else if (labyrinthe.J3T3.i == labyrinthe.tresor13.i && labyrinthe.J3T3.j == labyrinthe.tresor13.j){
	 img19.setAttribute("src","./image/ctresor13.png");
	}
	else if (labyrinthe.J3T3.i == labyrinthe.tresor14.i && labyrinthe.J3T3.j == labyrinthe.tresor14.j){
	 img19.setAttribute("src","./image/ctresor14.png");
	}
	else if (labyrinthe.J3T3.i == labyrinthe.tresor15.i && labyrinthe.J3T3.j == labyrinthe.tresor15.j){
	 img19.setAttribute("src","./image/ctresor15.png");
	}
	else if (labyrinthe.J3T3.i == labyrinthe.tresor16.i && labyrinthe.J3T3.j == labyrinthe.tresor16.j){
	 img19.setAttribute("src","./image/ctresor16.png");
	}
	 var img21 = document.createElement("img");
	 img21.setAttribute("STYLE","margin-left:-20px");
	 img21.setAttribute("STYLE","position:absolute");
	if(labyrinthe.J4T1.i == -2){
	 	img21.setAttribute("src","./image/carte.png");
	 	}
	 	 else if (labyrinthe.J4T1.i == labyrinthe.tresor0.i && labyrinthe.J4T1.j == labyrinthe.tresor0.j){
	 img21.setAttribute("src","./image/ctresor0.png");
	}
	else if (labyrinthe.J4T1.i == labyrinthe.tresor1.i && labyrinthe.J4T1.j == labyrinthe.tresor1.j){
	 img21.setAttribute("src","./image/ctresor1.png");
	}
	else if (labyrinthe.J4T1.i == labyrinthe.tresor2.i && labyrinthe.J4T1.j == labyrinthe.tresor2.j){
	 img21.setAttribute("src","./image/ctresor2.png");
	}
	else if (labyrinthe.J4T1.i == labyrinthe.tresor3.i && labyrinthe.J4T1.j == labyrinthe.tresor3.j){
	 img21.setAttribute("src","./image/ctresor3.png");
	}
	else if (labyrinthe.J4T1.i == labyrinthe.tresor4.i && labyrinthe.J4T1.j == labyrinthe.tresor4.j){
	 img21.setAttribute("src","./image/ctresor4.png");
	}
	else if (labyrinthe.J4T1.i == labyrinthe.tresor5.i && labyrinthe.J4T1.j == labyrinthe.tresor5.j){
	 img21.setAttribute("src","./image/ctresor5.png");
	}
	else if (labyrinthe.J4T1.i == labyrinthe.tresor6.i && labyrinthe.J4T1.j == labyrinthe.tresor6.j){
	 img21.setAttribute("src","./image/ctresor6.png");
	}
	else if (labyrinthe.J4T1.i == labyrinthe.tresor7.i && labyrinthe.J4T1.j == labyrinthe.tresor7.j){
	 img21.setAttribute("src","./image/ctresor7.png");
	}
	else if (labyrinthe.J4T1.i == labyrinthe.tresor8.i && labyrinthe.J4T1.j == labyrinthe.tresor8.j){
	 img21.setAttribute("src","./image/ctresor8.png");
	}
	else if (labyrinthe.J4T1.i == labyrinthe.tresor9.i && labyrinthe.J4T1.j == labyrinthe.tresor9.j){
	 img21.setAttribute("src","./image/ctresor9.png");
	}
	else if (labyrinthe.J4T1.i == labyrinthe.tresor10.i && labyrinthe.J4T1.j == labyrinthe.tresor10.j){
	 img21.setAttribute("src","./image/ctresor10.png");
	}
	else if (labyrinthe.J4T1.i == labyrinthe.tresor11.i && labyrinthe.J4T1.j == labyrinthe.tresor11.j){
	 img21.setAttribute("src","./image/ctresor11.png");
	}
	else if (labyrinthe.J4T1.i == labyrinthe.tresor12.i && labyrinthe.J4T1.j == labyrinthe.tresor12.j){
	 img21.setAttribute("src","./image/ctresor12.png");
	}
	else if (labyrinthe.J4T1.i == labyrinthe.tresor13.i && labyrinthe.J4T1.j == labyrinthe.tresor13.j){
	 img21.setAttribute("src","./image/ctresor13.png");
	}
	else if (labyrinthe.J4T1.i == labyrinthe.tresor14.i && labyrinthe.J4T1.j == labyrinthe.tresor14.j){
	 img21.setAttribute("src","./image/ctresor14.png");
	}
	else if (labyrinthe.J4T1.i == labyrinthe.tresor15.i && labyrinthe.J4T1.j == labyrinthe.tresor15.j){
	 img21.setAttribute("src","./image/ctresor15.png");
	}
	else if (labyrinthe.J4T1.i == labyrinthe.tresor16.i && labyrinthe.J4T1.j == labyrinthe.tresor16.j){
	 img21.setAttribute("src","./image/ctresor16.png");
	}
	 var img23 = document.createElement("img");
	 img23.setAttribute("STYLE","margin-left:-20px");
	 img23.setAttribute("STYLE","position:absolute");
	if(labyrinthe.J4T2.i == -2){
	 	img23.setAttribute("src","./image/carte.png");
	 	}
	 	 else if (labyrinthe.J4T2.i == labyrinthe.tresor0.i && labyrinthe.J4T2.j == labyrinthe.tresor0.j){
	 img23.setAttribute("src","./image/ctresor0.png");
	}
	else if (labyrinthe.J4T2.i == labyrinthe.tresor1.i && labyrinthe.J4T2.j == labyrinthe.tresor1.j){
	 img23.setAttribute("src","./image/ctresor1.png");
	}
	else if (labyrinthe.J4T2.i == labyrinthe.tresor2.i && labyrinthe.J4T2.j == labyrinthe.tresor2.j){
	 img23.setAttribute("src","./image/ctresor2.png");
	}
	else if (labyrinthe.J4T2.i == labyrinthe.tresor3.i && labyrinthe.J4T2.j == labyrinthe.tresor3.j){
	 img23.setAttribute("src","./image/ctresor3.png");
	}
	else if (labyrinthe.J4T2.i == labyrinthe.tresor4.i && labyrinthe.J4T2.j == labyrinthe.tresor4.j){
	 img23.setAttribute("src","./image/ctresor4.png");
	}
	else if (labyrinthe.J4T2.i == labyrinthe.tresor5.i && labyrinthe.J4T2.j == labyrinthe.tresor5.j){
	 img23.setAttribute("src","./image/ctresor5.png");
	}
	else if (labyrinthe.J4T2.i == labyrinthe.tresor6.i && labyrinthe.J4T2.j == labyrinthe.tresor6.j){
	 img23.setAttribute("src","./image/ctresor6.png");
	}
	else if (labyrinthe.J4T2.i == labyrinthe.tresor7.i && labyrinthe.J4T2.j == labyrinthe.tresor7.j){
	 img23.setAttribute("src","./image/ctresor7.png");
	}
	else if (labyrinthe.J4T2.i == labyrinthe.tresor8.i && labyrinthe.J4T2.j == labyrinthe.tresor8.j){
	 img23.setAttribute("src","./image/ctresor8.png");
	}
	else if (labyrinthe.J4T2.i == labyrinthe.tresor9.i && labyrinthe.J4T2.j == labyrinthe.tresor9.j){
	 img23.setAttribute("src","./image/ctresor9.png");
	}
	else if (labyrinthe.J4T2.i == labyrinthe.tresor10.i && labyrinthe.J4T2.j == labyrinthe.tresor10.j){
	 img23.setAttribute("src","./image/ctresor10.png");
	}
	else if (labyrinthe.J4T2.i == labyrinthe.tresor11.i && labyrinthe.J4T2.j == labyrinthe.tresor11.j){
	 img23.setAttribute("src","./image/ctresor11.png");
	}
	else if (labyrinthe.J4T2.i == labyrinthe.tresor12.i && labyrinthe.J4T2.j == labyrinthe.tresor12.j){
	 img23.setAttribute("src","./image/ctresor12.png");
	}
	else if (labyrinthe.J4T2.i == labyrinthe.tresor13.i && labyrinthe.J4T2.j == labyrinthe.tresor13.j){
	 img23.setAttribute("src","./image/ctresor13.png");
	}
	else if (labyrinthe.J4T2.i == labyrinthe.tresor14.i && labyrinthe.J4T2.j == labyrinthe.tresor14.j){
	 img23.setAttribute("src","./image/ctresor14.png");
	}
	else if (labyrinthe.J4T2.i == labyrinthe.tresor15.i && labyrinthe.J4T2.j == labyrinthe.tresor15.j){
	 img23.setAttribute("src","./image/ctresor15.png");
	}
	else if (labyrinthe.J4T2.i == labyrinthe.tresor16.i && labyrinthe.J4T2.j == labyrinthe.tresor16.j){
	 img23.setAttribute("src","./image/ctresor16.png");
	}
	var img24 = document.createElement("img");
	 img24.setAttribute("STYLE","margin-left:-20px");
	 img24.setAttribute("STYLE","position:absolute");
	if(labyrinthe.J4T3.i == -2){
	 	img24.setAttribute("src","./image/carte.png");
	 	}
	 	 else if (labyrinthe.J4T3.i == labyrinthe.tresor0.i && labyrinthe.J4T3.j == labyrinthe.tresor0.j){
	 img24.setAttribute("src","./image/ctresor0.png");
	}
	else if (labyrinthe.J4T3.i == labyrinthe.tresor1.i && labyrinthe.J4T3.j == labyrinthe.tresor1.j){
	 img24.setAttribute("src","./image/ctresor1.png");
	}
	else if (labyrinthe.J4T3.i == labyrinthe.tresor2.i && labyrinthe.J4T3.j == labyrinthe.tresor2.j){
	 img24.setAttribute("src","./image/ctresor2.png");
	}
	else if (labyrinthe.J4T3.i == labyrinthe.tresor3.i && labyrinthe.J4T3.j == labyrinthe.tresor3.j){
	 img24.setAttribute("src","./image/ctresor3.png");
	}
	else if (labyrinthe.J4T3.i == labyrinthe.tresor4.i && labyrinthe.J4T3.j == labyrinthe.tresor4.j){
	 img24.setAttribute("src","./image/ctresor4.png");
	}
	else if (labyrinthe.J4T3.i == labyrinthe.tresor5.i && labyrinthe.J4T3.j == labyrinthe.tresor5.j){
	 img24.setAttribute("src","./image/ctresor5.png");
	}
	else if (labyrinthe.J4T3.i == labyrinthe.tresor6.i && labyrinthe.J4T3.j == labyrinthe.tresor6.j){
	 img24.setAttribute("src","./image/ctresor6.png");
	}
	else if (labyrinthe.J4T3.i == labyrinthe.tresor7.i && labyrinthe.J4T3.j == labyrinthe.tresor7.j){
	 img24.setAttribute("src","./image/ctresor7.png");
	}
	else if (labyrinthe.J4T3.i == labyrinthe.tresor8.i && labyrinthe.J4T3.j == labyrinthe.tresor8.j){
	 img24.setAttribute("src","./image/ctresor8.png");
	}
	else if (labyrinthe.J4T3.i == labyrinthe.tresor9.i && labyrinthe.J4T3.j == labyrinthe.tresor9.j){
	 img24.setAttribute("src","./image/ctresor9.png");
	}
	else if (labyrinthe.J4T3.i == labyrinthe.tresor10.i && labyrinthe.J4T3.j == labyrinthe.tresor10.j){
	 img24.setAttribute("src","./image/ctresor10.png");
	}
	else if (labyrinthe.J4T3.i == labyrinthe.tresor11.i && labyrinthe.J4T3.j == labyrinthe.tresor11.j){
	 img24.setAttribute("src","./image/ctresor11.png");
	}
	else if (labyrinthe.J4T3.i == labyrinthe.tresor12.i && labyrinthe.J4T3.j == labyrinthe.tresor12.j){
	 img24.setAttribute("src","./image/ctresor12.png");
	}
	else if (labyrinthe.J4T3.i == labyrinthe.tresor13.i && labyrinthe.J4T3.j == labyrinthe.tresor13.j){
	 img24.setAttribute("src","./image/ctresor13.png");
	}
	else if (labyrinthe.J4T3.i == labyrinthe.tresor14.i && labyrinthe.J4T3.j == labyrinthe.tresor14.j){
	 img24.setAttribute("src","./image/ctresor14.png");
	}
	else if (labyrinthe.J4T3.i == labyrinthe.tresor15.i && labyrinthe.J4T3.j == labyrinthe.tresor15.j){
	 img24.setAttribute("src","./image/ctresor15.png");
	}
	else if (labyrinthe.J4T3.i == labyrinthe.tresor16.i && labyrinthe.J4T3.j == labyrinthe.tresor16.j){
	 img24.setAttribute("src","./image/ctresor16.png");
	}
	tdjb = document.createElement("TD");
	tdjv = document.createElement("TD");
	tdjr = document.createElement("TD");
	tdjj = document.createElement("TD");
	var imgjb = document.createElement("img");
	var imgjv = document.createElement("img");
	var imgjr = document.createElement("img");
	var imgjj = document.createElement("img");
	if (this.B == "True"){
		imgjb.setAttribute("src","./image/jbv.png");
		}
	else{
		imgjb.setAttribute("src","./image/jb.png");
		}
	if ((this.R == "True") && (this.nbrJ > 2)){
		imgjr.setAttribute("src","./image/jrv.png");
		}
	else if (this.nbrJ > 2){
		imgjr.setAttribute("src","./image/jr.png");
		}
	if ((this.V == "True") && (this.nbrJ > 1)){
		imgjv.setAttribute("src","./image/jvv.png");
		}
	else if (this.nbrJ > 1){
		imgjv.setAttribute("src","./image/jv.png");
		}
	if ((this.J == "True") && (this.nbrJ > 3)){
		imgjj.setAttribute("src","./image/jjv.png");
		}
	else if (this.nbrJ > 3){
		imgjj.setAttribute("src","./image/jj.png");
		}
	 tdjb.appendChild(imgjb);
	 tdjr.appendChild(imgjr);
	 tdjv.appendChild(imgjv);
	 tdjj.appendChild(imgjj);
	tr1 = document.createElement("TR");
	tr2 = document.createElement("TR");
	tr3 = document.createElement("TR");
	tr4 = document.createElement("TR");
	td1 = document.createElement("TD");
	td2 = document.createElement("TD");
	td3 = document.createElement("TD");
	td4 = document.createElement("TD");
	td5 = document.createElement("TD");
	td6 = document.createElement("TD");
	td7 = document.createElement("TD");
	td8 = document.createElement("TD");
	td9 = document.createElement("TD");
	td10 = document.createElement("TD");
	td11 = document.createElement("TD");
	td12 = document.createElement("TD");
	td1.appendChild(img1);
	td1.appendChild(img200);
	 td2.appendChild(img3);
	td2.appendChild(img400);
	 td3.appendChild(img5);
	td3.appendChild(img600);
	tr1.appendChild(tdjb);
	tr1.appendChild(td1);
	tr1.appendChild(td2);
	tr1.appendChild(td3);
	tabcarte.appendChild(tr1);
	if(this.nbrJ > 1){
		td4.appendChild(img7);
		td4.appendChild(img210);
	 	td5.appendChild(img9);
		td5.appendChild(img410);
	 	td6.appendChild(img11);
		td6.appendChild(img610);
		tr2.appendChild(tdjv);
		tr2.appendChild(td4);
		tr2.appendChild(td5);
		tr2.appendChild(td6);
		tabcarte.appendChild(tr2);
	}
	if(this.nbrJ > 2){
		td7.appendChild(img13);
		td7.appendChild(img220);
		td8.appendChild(img17);
		td8.appendChild(img420);
	 	td9.appendChild(img19);
		td9.appendChild(img620);
		tr3.appendChild(tdjr);
		tr3.appendChild(td7);
		tr3.appendChild(td8);
		tr3.appendChild(td9);
		tabcarte.appendChild(tr3);
	}
	if(this.nbrJ > 3){
		td10.appendChild(img21);
		td10.appendChild(img230);
	 	td11.appendChild(img23);
		td11.appendChild(img430);
	 	td12.appendChild(img24);
		td12.appendChild(img630);
		tr4.appendChild(tdjj);
		tr4.appendChild(td10);
		tr4.appendChild(td11);
		tr4.appendChild(td12);
		tabcarte.appendChild(tr4);
	 }
	// Mettre en évidence les cases accessibles :
  montrerAccessibles(this.pionB.i, this.pionB.j);
  // Effacer message d'état :
  	msg = document.getElementById("status-msg");
  msg.innerHTML = "";
  
  // Sauvegarder l'état du jeu :
  // puisque à chaque modification du labyrinthe on le doit afficher,
  // appeler la méthode de sauvegarde ici nous garantit que toute modification
  // sera immédiatement sauvegardée !
  this.sauvegarder();
}

function finit(){
if(labyrinthe.mouvement == "True" || labyrinthe.mouvementts != 2){
	var msg = document.getElementById("status-msg");
	msg.innerHTML = "Erreur : Vous devez d&rsquo;abord deplacer une case!";
	}
else{
labyrinthe.fait = "False";
	if (labyrinthe.nbrJ == 1){
		if(B == "True" && fait == "False"){ 
    		fait = "True";
 		}
 	}
	else if (labyrinthe.nbrJ == 2){
		if(labyrinthe.B == "True" && labyrinthe.fait == "False"){ 
    		labyrinthe.B = "False";
    		labyrinthe.V = "True";
    		labyrinthe.fait = "True";
 		}
 		else if (labyrinthe.V == "True" && labyrinthe.fait == "False"){ 
    		labyrinthe.V = "False";
    		labyrinthe.B = "True";
    		labyrinthe.fait = "True";
 		}
 	}
	else if (labyrinthe.nbrJ == 3){
		if(labyrinthe.B == "True" && labyrinthe.fait == "False"){ 
    		labyrinthe.B = "False";
    		labyrinthe.V = "True";
    		labyrinthe.fait = "True";
 		}
 		else if (labyrinthe.V == "True" && labyrinthe.fait == "False"){ 
    		labyrinthe.V = "False";
    		labyrinthe.R = "True";
    		labyrinthe.fait = "True";
 		}
 		else if (labyrinthe.R == "True" && labyrinthe.fait == "False"){ 
   		labyrinthe.R = "False";
    		labyrinthe.B = "True";
    		labyrinthe.fait = "True";
 		}
 	}
	else if (labyrinthe.nbrJ == 4){
		if(labyrinthe.B == "True" && labyrinthe.fait == "False"){ 
    		labyrinthe.B = "False";
    		labyrinthe.V = "True";
    		labyrinthe.fait = "True";
 		}
 		else if (labyrinthe.V == "True" && labyrinthe.fait == "False"){ 
    		labyrinthe.V = "False";
    		labyrinthe.R = "True";
    		labyrinthe.fait = "True";
 		}
 		else if (labyrinthe.R == "True" && labyrinthe.fait == "False"){ 
   		labyrinthe.R = "False";
    		labyrinthe.J = "True";
    		labyrinthe.fait = "True";
 		}
 		else if (labyrinthe.J == "True" && labyrinthe.fait == "False"){ 
    		labyrinthe.J = "False";
    		labyrinthe.B = "True";
    		labyrinthe.fait = "True";
 		}
 	}
 	labyrinthe.mouvement = "True";
	// Redessiner le tableau :
    labyrinthe.afficher();
	}
}
// Constructeur d'un objet "ensemble de cases accessibles"
// avec une méthode récursive spécialisée qui ajoute une
// case si non déjà présente et toutes ses voisines accessibles

function EnsAcc()
{
}
EnsAcc.prototype = Array.prototype;
// Méthode récursive utilisé pour déterminer l'ensemble accessible :
EnsAcc.prototype.add = function(i, j)
{
  var c = labyrinthe.cell(i, j);
	 if(this.indexOf(c) >= 0)
	   return;// case déjà visitée: o//n arrête là.
	 if(i == labyrinthe.pionB.i && j == labyrinthe.pionB.j && labyrinthe.B == "False")
	 	return;
	 if(i == labyrinthe.pionR.i && j == labyrinthe.pionR.j && labyrinthe.R == "False")
	 	return;
	 if(i == labyrinthe.pionV.i && j == labyrinthe.pionV.j && labyrinthe.V == "False")
	 	return;
	 if(i == labyrinthe.pionJ.i && j == labyrinthe.pionJ.j && labyrinthe.J == "False")
	 	return;
	 this.push(c);
	 var t = c.getType();
	 if((t.indexOf("N") != -1) && i>0)
	 {
	 	  adj = labyrinthe.cell(i - 1, j);
	 	  tAdj = adj.getType();
	 	  if(tAdj.indexOf("S") != -1)
	     this.add(i - 1, j);
	 }
	 if((t.indexOf("E") != -1) && j<labyrinthe.n - 1)
	 {
	 	  adj = labyrinthe.cell(i, j + 1);
	 	  tAdj = adj.getType();
	 	  if(tAdj.indexOf("O") != -1)
	     this.add(i, j + 1);
	 }
	 if((t.indexOf("S") != -1) && i<labyrinthe.n - 1)
	 {
	 	  adj = labyrinthe.cell(i + 1, j);
	 	  tAdj = adj.getType();
	 	  if(tAdj.indexOf("N") != -1)
	     this.add(i + 1, j);
	 }
	 if((t.indexOf("O") != -1) && j>0)
	 {
	 	  adj = labyrinthe.cell(i, j - 1);
	 	  tAdj = adj.getType();
	 	  if(tAdj.indexOf("E") != -1)
	     this.add(i, j - 1);
	 }
}
// Étant données les coordonnées d’une case initiale du labyrinthe,
// calcule l’ensemble des cases accessibles à partir de la case spécifiée.
Labyrinthe.prototype.ensembleAccessible = function(i, j)
{
	 casesAccessibles = new EnsAcc();
	 casesAccessibles.add(i, j);
	 return casesAccessibles;
}

// Mettre en évidence les cases accessibles à partir d'une case donnée :
function montrerAccessibles(i, j)
{
  var ensemble = labyrinthe.ensembleAccessible(i, j);
 
}

// Fonction de gestion des événements "clique" dans les cases :
function cliqueCase(i, j)
{
if (labyrinthe.gagnant == "True"){
	var msg = document.getElementById("status-msg");
	msg.innerHTML = "Erreur : Le jeu est finit merci d&rsquo;en recommencer un autre!";
	}
else if(labyrinthe.mouvement == "True" || labyrinthe.casebouge != 2){
	var msg = document.getElementById("status-msg");
	msg.innerHTML = "Erreur : Vous devez d&rsquo;abord deplacer une case!";
	}
else if(labyrinthe.mouvement == "False"){
	if (labyrinthe.B == "True"){ 
	 try
	 {
    var ensemble = labyrinthe.ensembleAccessible(labyrinthe.pionB.i, labyrinthe.pionB.j);
    if(ensemble.indexOf(labyrinthe.cell(i, j)) < 0)
      throw "Mouvement non valide !";
    if(labyrinthe.casebouge != 2)
    	throw "Vous devez d&rsquo;abord deplacer une case!";
	   labyrinthe.pionB.i = i;
	   labyrinthe.pionB.j = j;
	   labyrinthe.mouvementts = 2;
	   if (labyrinthe.J1T1.i == labyrinthe.pionB.i && labyrinthe.J1T1.j == labyrinthe.pionB.j){
	   	labyrinthe.J1T1.i = -2;
	   	labyrinthe.J1T1.j = -2;
	   	finit();
	   	}
	   else if (labyrinthe.J1T2.i == labyrinthe.pionB.i && labyrinthe.J1T2.j == labyrinthe.pionB.j && labyrinthe.J1T1.i == -2){
	   	labyrinthe.J1T2.i = -2;
	   	labyrinthe.J1T2.j = -2;
	   	finit();
	   	}
	   else if (labyrinthe.J1T3.i == labyrinthe.pionB.i && labyrinthe.J1T3.j == labyrinthe.pionB.j && labyrinthe.J1T2.i == -2){
	   	labyrinthe.J1T3.i = -2;
	   	labyrinthe.J1T3.j = -2;
	   	finit();
	   	}
	   	else if(labyrinthe.J1T3.i == -2 && 0 == labyrinthe.pionB.i && 0 == labyrinthe.pionB.j){
	   	var gagne = document.getElementById("gagne");
			gagne.innerHTML = "Joueur Bleu gagne";
			labyrinthe.gagnant = "True";
	   }
    // Redessiner le tableau :
    labyrinthe.afficher();
  }
  catch(err)
  {
  	  var msg = document.getElementById("status-msg");
    msg.innerHTML = "Erreur : " + err;
  }
  labyrinthe.masqueClique = true;
  }
  else if (labyrinthe.V == "True"){ 
	 try
	 {
    var ensemble = labyrinthe.ensembleAccessible(labyrinthe.pionV.i, labyrinthe.pionV.j);
    if(ensemble.indexOf(labyrinthe.cell(i, j)) < 0)
      throw "Mouvement non valide !";
	   labyrinthe.pionV.i = i;
	   labyrinthe.pionV.j = j;
	   if (labyrinthe.J2T1.i == labyrinthe.pionV.i && labyrinthe.J2T1.j == labyrinthe.pionV.j){
	   	labyrinthe.J2T1.i = -2;
	   	labyrinthe.J2T1.j = -2;
	   	finit();
	   	}
	   else if (labyrinthe.J2T2.i == labyrinthe.pionV.i && labyrinthe.J2T2.j == labyrinthe.pionV.j && labyrinthe.J2T1.i == -2){
	   	labyrinthe.J2T2.i = -2;
	   	labyrinthe.J2T2.j = -2;
	   	finit();
	   	}
	   else if (labyrinthe.J2T3.i == labyrinthe.pionV.i && labyrinthe.J2T3.j == labyrinthe.pionV.j && labyrinthe.J2T2.i == -2){
	   	labyrinthe.J2T3.i = -2;
	   	labyrinthe.J2T3.j = -2;
	   	finit();
	   	}
	   else if((labyrinthe.J2T3.i == -2) && (0 == labyrinthe.pionV.i) && ((labyrinthe.n - 1) == labyrinthe.pionV.j)){
	   	var gagne = document.getElementById("gagne");
			gagne.innerHTML = "Joueur Vert gagne";
			labyrinthe.gagnant = "True";
	   }
    // Redessiner le tableau :
    labyrinthe.afficher();
  }
  catch(err)
  {
  	  var msg = document.getElementById("status-msg");
    msg.innerHTML = "Erreur : " + err;
  }
  labyrinthe.masqueClique = true;}
  else if (labyrinthe.R == "True"){ 
	 try
	 {
    var ensemble = labyrinthe.ensembleAccessible(labyrinthe.pionR.i, labyrinthe.pionR.j);
    if(ensemble.indexOf(labyrinthe.cell(i, j)) < 0)
      throw "Mouvement non valide !";
	   labyrinthe.pionR.i = i;
	   labyrinthe.pionR.j = j;
	   if (labyrinthe.J3T1.i == labyrinthe.pionR.i && labyrinthe.J3T1.j == labyrinthe.pionR.j){
	   	labyrinthe.J3T1.i = -2;
	   	labyrinthe.J3T1.j = -2;
	   	finit();
	   	}
	   else if (labyrinthe.J3T2.i == labyrinthe.pionR.i && labyrinthe.J3T2.j == labyrinthe.pionR.j && labyrinthe.J3T1.i == -2){
	   	labyrinthe.J3T2.i = -2;
	   	labyrinthe.J3T2.j = -2;
	   	finit();
	   	}
	   else if (labyrinthe.J3T3.i == labyrinthe.pionR.i && labyrinthe.J3T3.j == labyrinthe.pionR.j && labyrinthe.J3T2.i == -2){
	   	labyrinthe.J3T3.i = -2;
	   	labyrinthe.J3T3.j = -2;
	   	finit();
	   	}
	   else if((labyrinthe.J3T3.i == -2) && ((labyrinthe.n - 1) == labyrinthe.pionR.i) && ((labyrinthe.n - 1) == labyrinthe.pionR.j)){
	   	var gagne = document.getElementById("gagne");
			gagne.innerHTML = "Joueur Rouge gagne";
			labyrinthe.gagnant = "True";
	   }
    // Redessiner le tableau :
    labyrinthe.afficher();
  }
  catch(err)
  {
  	  var msg = document.getElementById("status-msg");
    msg.innerHTML = "Erreur : " + err;
  }
  labyrinthe.masqueClique = true;}
  if (labyrinthe.J == "True"){ 
	 try
	 {
    var ensemble = labyrinthe.ensembleAccessible(labyrinthe.pionJ.i, labyrinthe.pionJ.j);
    if(ensemble.indexOf(labyrinthe.cell(i, j)) < 0)
      throw "Mouvement non valide !";
	   labyrinthe.pionJ.i = i;
	   labyrinthe.pionJ.j = j;
	   if (labyrinthe.J4T1.i == labyrinthe.pionJ.i && labyrinthe.J4T1.j == labyrinthe.pionJ.j){
	   	labyrinthe.J4T1.i = -2;
	   	labyrinthe.J4T1.j = -2;
	   	finit();
	   	}
	   else if (labyrinthe.J4T2.i == labyrinthe.pionJ.i && labyrinthe.J4T2.j == labyrinthe.pionJ.j && labyrinthe.J4T1.i == -2){
	   	labyrinthe.J4T2.i = -2;
	   	labyrinthe.J4T2.j = -2;
	   	finit();
	   	}
	   else if (labyrinthe.J4T3.i == labyrinthe.pionJ.i && labyrinthe.J4T3.j == labyrinthe.pionJ.j && labyrinthe.J4T2.i == -2){
	   	labyrinthe.J4T3.i = -2;
	   	labyrinthe.J4T3.j = -2;
	   	finit();
	   }
	   else if((labyrinthe.J4T3.i == -2) && ((labyrinthe.n - 1) == labyrinthe.pionJ.i) && (0 == labyrinthe.pionJ.j)){
	   	var gagne = document.getElementById("gagne");
			gagne.innerHTML = "Joueur Jaune gagne";
			labyrinthe.gagnant = "True";
	   }
    // Redessiner le tableau :
    labyrinthe.afficher();
  }
  catch(err)
  {
  	  var msg = document.getElementById("status-msg");
    msg.innerHTML = "Erreur : " + err;
  }
  labyrinthe.masqueClique = true;}
}
}
// Désélectionner l'ensemble accessible, si mis en évidence :
function deselect()
{
	 if(labyrinthe.masqueClique)
	   labyrinthe.masqueClique = false;
	 else
	   labyrinthe.afficher();
}

Labyrinthe.prototype.fromJSON = function(s)
{
	 o = JSON.parse(s);
	 this.n = o.n;
	 this.cases = new Array(this.n*this.n);
	 // Remplissons le tableau...
	 for(i = 0; i<this.n*this.n; i++)
	 {
	   this.cases[i] = new Tuile("NS");
	   this.cases[i].fromJSON(o.cases[i]);
	 }
	 this.tuileEnTrop = new Tuile("NS");
	 this.tuileEnTrop.fromJSON(o.tuileEnTrop);
	 this.provenanceTuileEnTrop = o.provenanceTuileEnTrop;
	 this.pionB = o.pionB;
	 this.pionV = o.pionV;
	 this.pionJ = o.pionJ;
    this.pionR = o.pionR;
	 this.B = o.B;
	 this.V = o.V;
	 this.R = o.R;
	 this.J = o.J;
	 this.fait = o.fait;
	 this.mouvement = o.mouvement;
	 this.mouvementts = o.mouvementts;
	 this.gagnant = o.gagnant;
	 this.casebouge = o.casebouge;
	 this.nbrJ = o.nbrJ;
	 this.tresor0 = o.tresor0;
	 this.tresor1 = o.tresor1;
	 this.tresor2 = o.tresor2;
	 this.tresor3 = o.tresor3;
	 this.tresor4 = o.tresor4;
	 this.tresor5 = o.tresor5;
	 this.tresor6 = o.tresor6;
	 this.tresor7 = o.tresor7;
	 this.tresor8 = o.tresor8;
	 this.tresor9 = o.tresor9;
	 this.tresor10 = o.tresor10;
	 this.tresor11 = o.tresor11;
	 this.tresor12 = o.tresor12;
	 this.tresor13 = o.tresor13;
	 this.tresor14 = o.tresor14;
	 this.tresor15 = o.tresor15;
	 this.tresor16 = o.tresor16;
	 this.J1T1 = o.J1T1;
	 this.J1T2 = o.J1T2;
	 this.J1T3 = o.J1T3;
	 this.J2T1 = o.J2T1;
	 this.J2T2 = o.J2T2;
	 this.J2T3 = o.J2T3;
	 this.J3T1 = o.J3T1;
	 this.J3T2 = o.J3T2;
	 this.J3T3 = o.J3T3;
	 this.J4T1 = o.J4T1;
	 this.J4T2 = o.J4T2;
	 this.J4T3 = o.J4T3;
	 this.J1T1.i = o.J1T1.i;
	 this.J1T2.i = o.J1T2.i;
	 this.J1T3.i = o.J1T3.i;
	 this.J2T1.i = o.J2T1.i;
	 this.J2T2.i = o.J2T2.i;
	 this.J2T3.i = o.J2T3.i;
	 this.J3T1.i = o.J3T1.i;
	 this.J3T2.i = o.J3T2.i;
	 this.J3T3.i = o.J3T3.i;
	 this.J4T1.i = o.J4T1.i;
	 this.J4T2.i = o.J4T2.i;
	 this.J4T3.i = o.J4T3.i;
	 this.J1T1.j = o.J1T1.j;
	 this.J1T2.j = o.J1T2.j;
	 this.J1T3.j = o.J1T3.j;
	 this.J2T1.j = o.J2T1.j;
	 this.J2T2.j = o.J2T2.j;
	 this.J2T3.j = o.J2T3.j;
	 this.J3T1.j = o.J3T1.j;
	 this.J3T2.j = o.J3T2.j;
	 this.J3T3.j = o.J3T3.j;
	 this.J4T1.j = o.J4T1.j;
	 this.J4T2.j = o.J4T2.j;
	 this.J4T3.j = o.J4T3.j;
}

// Sauvegarde de l'état du jeu dans l'aire de stockage locale
Labyrinthe.prototype.sauvegarder = function()
{
	 localStorage.setItem("labyrinthe", JSON.stringify(labyrinthe));
}

// Traitement d'un clique sur la tuile en trop :
function cliqueTuileEnTrop()
{
	 labyrinthe.tuileEnTrop.pivoter90();
	 labyrinthe.afficher();
}

// Insertion de la tuile en trop dans le plan de jeu :
Labyrinthe.prototype.pousser = function(i, j)
{	
	labyrinthe.mouvement = "False";
	labyrinthe.casebouge = 2;
	 if(i == this.provenanceTuileEnTrop.i && j == this.provenanceTuileEnTrop.j){
    throw new Error("La tuile en trop ne peut pas &ecirc;tre repouss&eacute;e dans la case d&rsquo;o&ugrave; elle provient!");
	labyrinthe.mouvement = "True"; 
 }
  // Calculer les coefficients horizontal (dj) et vertical (di) des déplacements :
  if(i == 0 && j%2 == 1)
  {
  	  di = 1; dj = 0;
  	}
  	else if(i == this.n - 1 && j%2 == 1)
  	{
  	  di = -1; dj = 0;
  	}
  	else if(j == 0 && i%2 == 1)
  	{
  	  di = 0; dj = 1;
  	}
  	else if(j == this.n - 1 && i%2 == 1)
  	{
  	  di = 0; dj = -1;
  	}
  	else{
    throw new Error("La tuile en trop ne peut pas &ecirc;tre repouss&eacute;e dans la case (" + i + ", " + j + ")!");
    labyrinthe.mouvement = "True";
 }
  // Effectuer tous les déplacements :
  this.provenanceTuileEnTrop = new Object();
  this.provenanceTuileEnTrop.i = i + (this.n - 1)*di;
  this.provenanceTuileEnTrop.j = j + (this.n - 1)*dj;
  nouvelleTuileEnTrop = this.cell(this.provenanceTuileEnTrop.i, this.provenanceTuileEnTrop.j);
  for(k = this.n - 1; k>0; k--)
    this.cell(i + k*di, j + k*dj, this.cell(i + (k - 1)*di, j + (k - 1)*dj));
  this.cell(i, j, this.tuileEnTrop);
  this.tuileEnTrop = nouvelleTuileEnTrop;
  
  // Gérer déplacement du pion :
  if(di == 0 && i == this.pionB.i)
    this.pionB.j = (this.pionB.j + this.n + dj) % this.n; 
  else if(dj == 0 && j == this.pionB.j)
    this.pionB.i = (this.pionB.i + this.n + di) % this.n; 
  if(di == 0 && i == this.pionV.i)
    this.pionV.j = (this.pionV.j + this.n + dj) % this.n; 
  else if(dj == 0 && j == this.pionV.j)
    this.pionV.i = (this.pionV.i + this.n + di) % this.n;
  if(di == 0 && i == this.pionR.i)
    this.pionR.j = (this.pionR.j + this.n + dj) % this.n; 
  else if(dj == 0 && j == this.pionR.j)
    this.pionR.i = (this.pionR.i + this.n + di) % this.n;
  if(di == 0 && i == this.pionJ.i)
    this.pionJ.j = (this.pionJ.j + this.n + dj) % this.n; 
  else if(dj == 0 && j == this.pionJ.j)
    this.pionJ.i = (this.pionJ.i + this.n + di) % this.n; 
   if ((labyrinthe.tresor0.i == -1) && (labyrinthe.tresor0.j == -1)){
	 labyrinthe.tresor0.j = j - dj;
    labyrinthe.tresor0.i = i - di;
 	}
 	if ((labyrinthe.tresor1.i == -1) && (labyrinthe.tresor1.j == -1)){
	 labyrinthe.tresor1.j = j - dj;
    labyrinthe.tresor1.i = i - di;
 	}
 	if ((labyrinthe.tresor2.i == -1) && (labyrinthe.tresor2.j == -1)){
	 labyrinthe.tresor2.j = j - dj;
    labyrinthe.tresor2.i = i - di;
 	}
 	if ((labyrinthe.tresor3.i == -1) && (labyrinthe.tresor3.j == -1)){
	 labyrinthe.tresor3.j = j - dj;
    labyrinthe.tresor3.i = i - di;
 	}
 	if ((labyrinthe.tresor4.i == -1) && (labyrinthe.tresor4.j == -1)){
	 labyrinthe.tresor4.j = j - dj;
    labyrinthe.tresor4.i = i - di;
 	}
 	if ((labyrinthe.tresor5.i == -1) && (labyrinthe.tresor5.j == -1)){
	 labyrinthe.tresor5.j = j - dj;
    labyrinthe.tresor5.i = i - di;
 	}
 	if ((labyrinthe.tresor6.i == -1) && (labyrinthe.tresor6.j == -1)){
	 labyrinthe.tresor6.j = j - dj;
    labyrinthe.tresor6.i = i - di;
 	}
 	if ((labyrinthe.tresor7.i == -1) && (labyrinthe.tresor7.j == -1)){
	 labyrinthe.tresor7.j = j - dj;
    labyrinthe.tresor7.i = i - di;
 	}
 	if ((labyrinthe.tresor8.i == -1) && (labyrinthe.tresor8.j == -1)){
	 labyrinthe.tresor8.j = j - dj;
    labyrinthe.tresor8.i = i - di;
 	}
 	if ((labyrinthe.tresor9.i == -1) && (labyrinthe.tresor9.j == -1)){
	 labyrinthe.tresor9.j = j - dj;
    labyrinthe.tresor9.i = i - di;
 	}
 	if ((labyrinthe.tresor10.i == -1) && (labyrinthe.tresor10.j == -1)){
	 labyrinthe.tresor10.j = j - dj;
    labyrinthe.tresor10.i = i - di;
 	}
 	if ((labyrinthe.tresor11.i == -1) && (labyrinthe.tresor11.j == -1)){
	 labyrinthe.tresor11.j = j - dj;
    labyrinthe.tresor11.i = i - di;
 	}
 	if ((labyrinthe.tresor12.i == -1) && (labyrinthe.tresor12.j == -1)){
	 labyrinthe.tresor12.j = j - dj;
    labyrinthe.tresor12.i = i - di;
 	}
 	if ((labyrinthe.tresor13.i == -1) && (labyrinthe.tresor13.j == -1)){
	 labyrinthe.tresor13.j = j - dj;
    labyrinthe.tresor13.i = i - di;
 	}
 	if ((labyrinthe.tresor14.i == -1) && (labyrinthe.tresor14.j == -1)){
	 labyrinthe.tresor14.j = j - dj;
    labyrinthe.tresor14.i = i - di;
 	}
 	if ((labyrinthe.tresor15.i == -1) && (labyrinthe.tresor15.j == -1)){
	 labyrinthe.tresor15.j = j - dj;
    labyrinthe.tresor15.i = i - di;
 	}
 	if ((labyrinthe.tresor16.i == -1) && (labyrinthe.tresor16.j == -1)){
	 labyrinthe.tresor16.j = j - dj;
    labyrinthe.tresor16.i = i - di;
 	}
if(dj == 0 && j == labyrinthe.tresor0.j){
  if (labyrinthe.tresor0.i == 0 && di == -1){
  	 labyrinthe.tresor0.j = -1;
    labyrinthe.tresor0.i = -1;
  	}
  	else if (labyrinthe.tresor0.i == (this.n - 1) && di == 1){
  	 labyrinthe.tresor0.j = -1;
    labyrinthe.tresor0.i = -1;
  	}  
  else {
    	labyrinthe.tresor0.i = labyrinthe.tresor0.i + di;
 		}
 }
  if(di == 0 && i == labyrinthe.tresor0.i){
  if (labyrinthe.tresor0.j == 0 && dj == -1){
  	 labyrinthe.tresor0.j = -1;
    labyrinthe.tresor0.i = -1;
  	}
  	else if (labyrinthe.tresor0.j == (this.n - 1) && dj == 1){
  	 labyrinthe.tresor0.j = -1;
    labyrinthe.tresor0.i = -1;
  	}  
  else {
    	labyrinthe.tresor0.j = labyrinthe.tresor0.j + dj;
 		}
 }
 if(dj == 0 && j == labyrinthe.tresor1.j){
  if (labyrinthe.tresor1.i == 0 && di == -1){
  	 labyrinthe.tresor1.j = -1;
    labyrinthe.tresor1.i = -1;
  	}
  	else if (labyrinthe.tresor1.i == (this.n - 1) && di == 1){
  	 labyrinthe.tresor1.j = -1;
    labyrinthe.tresor1.i = -1;
  	}  
  else {
    	labyrinthe.tresor1.i = labyrinthe.tresor1.i + di;
 		}
 }
  if(di == 0 && i == labyrinthe.tresor1.i){
  if (labyrinthe.tresor1.j == 0 && dj == -1){
  	 labyrinthe.tresor1.j = -1;
    labyrinthe.tresor1.i = -1;
  	}
  	else if (labyrinthe.tresor1.j == (this.n - 1) && dj == 1){
  	 labyrinthe.tresor1.j = -1;
    labyrinthe.tresor1.i = -1;
  	}  
  else {
    	labyrinthe.tresor1.j = labyrinthe.tresor1.j + dj;
 		}
 }
  if(dj == 0 && j == labyrinthe.tresor2.j){
  if (labyrinthe.tresor2.i == 0 && di == -1){
  	 labyrinthe.tresor2.j = -1;
    labyrinthe.tresor2.i = -1;
  	}
  	else if (labyrinthe.tresor2.i == (this.n - 1) && di == 1){
  	 labyrinthe.tresor2.j = -1;
    labyrinthe.tresor2.i = -1;
  	}  
  else {
    	labyrinthe.tresor2.i = labyrinthe.tresor2.i + di;
 		}
 }
  if(di == 0 && i == labyrinthe.tresor2.i){
  if (labyrinthe.tresor2.j == 0 && dj == -1){
  	 labyrinthe.tresor2.j = -1;
    labyrinthe.tresor2.i = -1;
  	}
  	else if (labyrinthe.tresor2.j == (this.n - 1) && dj == 1){
  	 labyrinthe.tresor2.j = -1;
    labyrinthe.tresor2.i = -1;
  	}  
  else {
    	labyrinthe.tresor2.j = labyrinthe.tresor2.j + dj;
 		}
 }
  if(dj == 0 && j == labyrinthe.tresor3.j){
  if (labyrinthe.tresor3.i == 0 && di == -1){
  	 labyrinthe.tresor3.j = -1;
    labyrinthe.tresor3.i = -1;
  	}
  	else if (labyrinthe.tresor3.i == (this.n - 1) && di == 1){
  	 labyrinthe.tresor3.j = -1;
    labyrinthe.tresor3.i = -1;
  	}  
  else {
    	labyrinthe.tresor3.i = labyrinthe.tresor3.i + di;
 		}
 }
  if(di == 0 && i == labyrinthe.tresor3.i){
  if (labyrinthe.tresor3.j == 0 && dj == -1){
  	 labyrinthe.tresor3.j = -1;
    labyrinthe.tresor3.i = -1;
  	}
  	else if (labyrinthe.tresor3.j == (this.n - 1) && dj == 1){
  	 labyrinthe.tresor3.j = -1;
    labyrinthe.tresor3.i = -1;
  	}  
  else {
    	labyrinthe.tresor3.j = labyrinthe.tresor3.j + dj;
 		}
 }
  if(dj == 0 && j == labyrinthe.tresor4.j){
  if (labyrinthe.tresor4.i == 0 && di == -1){
  	 labyrinthe.tresor4.j = -1;
    labyrinthe.tresor4.i = -1;
  	}
  	else if (labyrinthe.tresor4.i == (this.n - 1) && di == 1){
  	 labyrinthe.tresor4.j = -1;
    labyrinthe.tresor4.i = -1;
  	}  
  else {
    	labyrinthe.tresor4.i = labyrinthe.tresor4.i + di;
 		}
 }
  if(di == 0 && i == labyrinthe.tresor4.i){
  if (labyrinthe.tresor4.j == 0 && dj == -1){
  	 labyrinthe.tresor4.j = -1;
    labyrinthe.tresor4.i = -1;
  	}
  	else if (labyrinthe.tresor4.j == (this.n - 1) && dj == 1){
  	 labyrinthe.tresor4.j = -1;
    labyrinthe.tresor4.i = -1;
  	}  
  else {
    	labyrinthe.tresor4.j = labyrinthe.tresor4.j + dj;
 		}
 }
  if(dj == 0 && j == labyrinthe.tresor5.j){
  if (labyrinthe.tresor5.i == 0 && di == -1){
  	 labyrinthe.tresor5.j = -1;
    labyrinthe.tresor5.i = -1;
  	}
  	else if (labyrinthe.tresor5.i == (this.n - 1) && di == 1){
  	 labyrinthe.tresor5.j = -1;
    labyrinthe.tresor5.i = -1;
  	}  
  else {
    	labyrinthe.tresor5.i = labyrinthe.tresor5.i + di;
 		}
 }
  if(di == 0 && i == labyrinthe.tresor5.i){
  if (labyrinthe.tresor5.j == 0 && dj == -1){
  	 labyrinthe.tresor5.j = -1;
    labyrinthe.tresor5.i = -1;
  	}
  	else if (labyrinthe.tresor5.j == (this.n - 1) && dj == 1){
  	 labyrinthe.tresor5.j = -1;
    labyrinthe.tresor5.i = -1;
  	}  
  else {
    	labyrinthe.tresor5.j = labyrinthe.tresor5.j + dj;
 		}
 }
  if(dj == 0 && j == labyrinthe.tresor6.j){
  if (labyrinthe.tresor6.i == 0 && di == -1){
  	 labyrinthe.tresor6.j = -1;
    labyrinthe.tresor6.i = -1;
  	}
  	else if (labyrinthe.tresor6.i == (this.n - 1) && di == 1){
  	 labyrinthe.tresor6.j = -1;
    labyrinthe.tresor6.i = -1;
  	}  
  else {
    	labyrinthe.tresor6.i = labyrinthe.tresor6.i + di;
 		}
 }
  if(di == 0 && i == labyrinthe.tresor6.i){
  if (labyrinthe.tresor6.j == 0 && dj == -1){
  	 labyrinthe.tresor6.j = -1;
    labyrinthe.tresor6.i = -1;
  	}
  	else if (labyrinthe.tresor6.j == (this.n - 1) && dj == 1){
  	 labyrinthe.tresor6.j = -1;
    labyrinthe.tresor6.i = -1;
  	}  
  else {
    	labyrinthe.tresor6.j = labyrinthe.tresor6.j + dj;
 		}
 }
  if(dj == 0 && j == labyrinthe.tresor7.j){
  if (labyrinthe.tresor7.i == 0 && di == -1){
  	 labyrinthe.tresor7.j = -1;
    labyrinthe.tresor7.i = -1;
  	}
  	else if (labyrinthe.tresor7.i == (this.n - 1) && di == 1){
  	 labyrinthe.tresor7.j = -1;
    labyrinthe.tresor7.i = -1;
  	}  
  else {
    	labyrinthe.tresor7.i = labyrinthe.tresor7.i + di;
 		}
 }
  if(di == 0 && i == labyrinthe.tresor7.i){
  if (labyrinthe.tresor7.j == 0 && dj == -1){
  	 labyrinthe.tresor7.j = -1;
    labyrinthe.tresor7.i = -1;
  	}
  	else if (labyrinthe.tresor7.j == (this.n - 1) && dj == 1){
  	 labyrinthe.tresor7.j = -1;
    labyrinthe.tresor7.i = -1;
  	}  
  else {
    	labyrinthe.tresor7.j = labyrinthe.tresor7.j + dj;
 		}
 }
  if(dj == 0 && j == labyrinthe.tresor8.j){
  if (labyrinthe.tresor8.i == 0 && di == -1){
  	 labyrinthe.tresor8.j = -1;
    labyrinthe.tresor8.i = -1;
  	}
  	else if (labyrinthe.tresor8.i == (this.n - 1) && di == 1){
  	 labyrinthe.tresor8.j = -1;
    labyrinthe.tresor8.i = -1;
  	}  
  else {
    	labyrinthe.tresor8.i = labyrinthe.tresor8.i + di;
 		}
 }
  if(di == 0 && i == labyrinthe.tresor8.i){
  if (labyrinthe.tresor8.j == 0 && dj == -1){
  	 labyrinthe.tresor8.j = -1;
    labyrinthe.tresor8.i = -1;
  	}
  	else if (labyrinthe.tresor8.j == (this.n - 1) && dj == 1){
  	 labyrinthe.tresor8.j = -1;
    labyrinthe.tresor8.i = -1;
  	}  
  else {
    	labyrinthe.tresor8.j = labyrinthe.tresor8.j + dj;
 		}
 }
  if(dj == 0 && j == labyrinthe.tresor9.j){
  if (labyrinthe.tresor9.i == 0 && di == -1){
  	 labyrinthe.tresor9.j = -1;
    labyrinthe.tresor9.i = -1;
  	}
  	else if (labyrinthe.tresor9.i == (this.n - 1) && di == 1){
  	 labyrinthe.tresor9.j = -1;
    labyrinthe.tresor9.i = -1;
  	}  
  else {
    	labyrinthe.tresor9.i = labyrinthe.tresor9.i + di;
 		}
 }
  if(di == 0 && i == labyrinthe.tresor9.i){
  if (labyrinthe.tresor9.j == 0 && dj == -1){
  	 labyrinthe.tresor9.j = -1;
    labyrinthe.tresor9.i = -1;
  	}
  	else if (labyrinthe.tresor9.j == (this.n - 1) && dj == 1){
  	 labyrinthe.tresor9.j = -1;
    labyrinthe.tresor9.i = -1;
  	}  
  else {
    	labyrinthe.tresor9.j = labyrinthe.tresor9.j + dj;
 		}
 }
  if(dj == 0 && j == labyrinthe.tresor10.j){
  if (labyrinthe.tresor10.i == 0 && di == -1){
  	 labyrinthe.tresor10.j = -1;
    labyrinthe.tresor10.i = -1;
  	}
  	else if (labyrinthe.tresor10.i == (this.n - 1) && di == 1){
  	 labyrinthe.tresor10.j = -1;
    labyrinthe.tresor10.i = -1;
  	}  
  else {
    	labyrinthe.tresor10.i = labyrinthe.tresor10.i + di;
 		}
 }
  if(di == 0 && i == labyrinthe.tresor10.i){
  if (labyrinthe.tresor10.j == 0 && dj == -1){
  	 labyrinthe.tresor10.j = -1;
    labyrinthe.tresor10.i = -1;
  	}
  	else if (labyrinthe.tresor10.j == (this.n - 1) && dj == 1){
  	 labyrinthe.tresor10.j = -1;
    labyrinthe.tresor10.i = -1;
  	}  
  else {
    	labyrinthe.tresor10.j = labyrinthe.tresor10.j + dj;
 		}
 }
  if(dj == 0 && j == labyrinthe.tresor11.j){
  if (labyrinthe.tresor11.i == 0 && di == -1){
  	 labyrinthe.tresor11.j = -1;
    labyrinthe.tresor11.i = -1;
  	}
  	else if (labyrinthe.tresor11.i == (this.n - 1) && di == 1){
  	 labyrinthe.tresor11.j = -1;
    labyrinthe.tresor11.i = -1;
  	}  
  else {
    	labyrinthe.tresor11.i = labyrinthe.tresor11.i + di;
 		}
 }
  if(di == 0 && i == labyrinthe.tresor11.i){
  if (labyrinthe.tresor11.j == 0 && dj == -1){
  	 labyrinthe.tresor11.j = -1;
    labyrinthe.tresor11.i = -1;
  	}
  	else if (labyrinthe.tresor11.j == (this.n - 1) && dj == 1){
  	 labyrinthe.tresor11.j = -1;
    labyrinthe.tresor11.i = -1;
  	}  
  else {
    	labyrinthe.tresor11.j = labyrinthe.tresor11.j + dj;
 		}
 }
  if(dj == 0 && j == labyrinthe.tresor12.j){
  if (labyrinthe.tresor12.i == 0 && di == -1){
  	 labyrinthe.tresor12.j = -1;
    labyrinthe.tresor12.i = -1;
  	}
  	else if (labyrinthe.tresor12.i == (this.n - 1) && di == 1){
  	 labyrinthe.tresor12.j = -1;
    labyrinthe.tresor12.i = -1;
  	}  
  else {
    	labyrinthe.tresor12.i = labyrinthe.tresor12.i + di;
 		}
 }
  if(di == 0 && i == labyrinthe.tresor12.i){
  if (labyrinthe.tresor12.j == 0 && dj == -1){
  	 labyrinthe.tresor12.j = -1;
    labyrinthe.tresor12.i = -1;
  	}
  	else if (labyrinthe.tresor12.j == (this.n - 1) && dj == 1){
  	 labyrinthe.tresor12.j = -1;
    labyrinthe.tresor12.i = -1;
  	}  
  else {
    	labyrinthe.tresor12.j = labyrinthe.tresor12.j + dj;
 		}
 }
  if(dj == 0 && j == labyrinthe.tresor13.j){
  if (labyrinthe.tresor13.i == 0 && di == -1){
  	 labyrinthe.tresor13.j = -1;
    labyrinthe.tresor13.i = -1;
  	}
  	else if (labyrinthe.tresor13.i == (this.n - 1) && di == 1){
  	 labyrinthe.tresor13.j = -1;
    labyrinthe.tresor13.i = -1;
  	}  
  else {
    	labyrinthe.tresor13.i = labyrinthe.tresor13.i + di;
 		}
 }
  if(di == 0 && i == labyrinthe.tresor13.i){
  if (labyrinthe.tresor13.j == 0 && dj == -1){
  	 labyrinthe.tresor13.j = -1;
    labyrinthe.tresor13.i = -1;
  	}
  	else if (labyrinthe.tresor13.j == (this.n - 1) && dj == 1){
  	 labyrinthe.tresor13.j = -1;
    labyrinthe.tresor13.i = -1;
  	}  
  else {
    	labyrinthe.tresor13.j = labyrinthe.tresor13.j + dj;
 		}
 }
  if(dj == 0 && j == labyrinthe.tresor14.j){
  if (labyrinthe.tresor14.i == 0 && di == -1){
  	 labyrinthe.tresor14.j = -1;
    labyrinthe.tresor14.i = -1;
  	}
  	else if (labyrinthe.tresor14.i == (this.n - 1) && di == 1){
  	 labyrinthe.tresor14.j = -1;
    labyrinthe.tresor14.i = -1;
  	}  
  else {
    	labyrinthe.tresor14.i = labyrinthe.tresor14.i + di;
 		}
 }
  if(di == 0 && i == labyrinthe.tresor14.i){
  if (labyrinthe.tresor14.j == 0 && dj == -1){
  	 labyrinthe.tresor14.j = -1;
    labyrinthe.tresor14.i = -1;
  	}
  	else if (labyrinthe.tresor14.j == (this.n - 1) && dj == 1){
  	 labyrinthe.tresor14.j = -1;
    labyrinthe.tresor14.i = -1;
  	}  
  else {
    	labyrinthe.tresor14.j = labyrinthe.tresor14.j + dj;
 		}
 }
  if(dj == 0 && j == labyrinthe.tresor15.j){
  if (labyrinthe.tresor15.i == 0 && di == -1){
  	 labyrinthe.tresor15.j = -1;
    labyrinthe.tresor15.i = -1;
  	}
  	else if (labyrinthe.tresor15.i == (this.n - 1) && di == 1){
  	 labyrinthe.tresor15.j = -1;
    labyrinthe.tresor15.i = -1;
  	}  
  else {
    	labyrinthe.tresor15.i = labyrinthe.tresor15.i + di;
 		}
 }
  if(di == 0 && i == labyrinthe.tresor15.i){
  if (labyrinthe.tresor15.j == 0 && dj == -1){
  	 labyrinthe.tresor15.j = -1;
    labyrinthe.tresor15.i = -1;
  	}
  	else if (labyrinthe.tresor15.j == (this.n - 1) && dj == 1){
  	 labyrinthe.tresor15.j = -1;
    labyrinthe.tresor15.i = -1;
  	}  
  else {
    	labyrinthe.tresor15.j = labyrinthe.tresor15.j + dj;
 		}
 }
  if(dj == 0 && j == labyrinthe.tresor16.j){
  if (labyrinthe.tresor16.i == 0 && di == -1){
  	 labyrinthe.tresor16.j = -1;
    labyrinthe.tresor16.i = -1;
  	}
  	else if (labyrinthe.tresor16.i == (this.n - 1) && di == 1){
  	 labyrinthe.tresor16.j = -1;
    labyrinthe.tresor16.i = -1;
  	}  
  else {
    	labyrinthe.tresor16.i = labyrinthe.tresor16.i + di;
 		}
 }
  if(di == 0 && i == labyrinthe.tresor16.i){
  if (labyrinthe.tresor16.j == 0 && dj == -1){
  	 labyrinthe.tresor16.j = -1;
    labyrinthe.tresor16.i = -1;
  	}
  	else if (labyrinthe.tresor16.j == (this.n - 1) && dj == 1){
  	 labyrinthe.tresor16.j = -1;
    labyrinthe.tresor16.i = -1;
  	}  
  else {
    	labyrinthe.tresor16.j = labyrinthe.tresor16.j + dj;
 		}
 }
}

//////////////////////////////////////////////////////////////////////
// Traitement du glisser-déposer

function allowDrop(ev)
{
  ev.preventDefault();
}

function drag(ev)
{
  ev.dataTransfer.setData("Text", ev.target.id);
}

function drop(ev, i, j)
{
  ev.preventDefault();
  var data = ev.dataTransfer.getData("Text");
  try
  {
  	if(labyrinthe.mouvement == "False"){
	throw new Error("Vous avez d&eacute;j&agrave; d&eacute;placer une case pendant ce tour!");
	}
else{
    labyrinthe.pousser(i, j);
 }
    labyrinthe.afficher();
  }
  catch(err)
  {
    msg = document.getElementById("status-msg");
    msg.innerHTML = err;
  	}
}